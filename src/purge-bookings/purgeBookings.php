<?php
include_once '../Helpers.php';
include_once 'repository.php';
include_once 'Constants.php';

$conn     = Helpers::initDB('OPTEST');
$bookings = [];

$type     = 'delete';
$brand    = 563;

// Get all the itineraries
$bookingData   = getBookingData($conn, $brand, $bookings);
$allOwners     = getAllOwners($conn, $brand);
$allProperties = getAllProperties($conn, $brand);

$owners      = ! empty($allOwners) ? array_column($allOwners, '__pk') : [];
$properties  = ! empty($allProperties) ? array_column($allProperties, '__pk') : [];
$itineraries = ! empty($bookingData) ? array_column($bookingData, 'nitineraryid') : [];
$customers   = ! empty($bookingData) ? array_column($bookingData, 'npersonid') : [];
$sessions    = ! empty($bookingData) ? array_column($bookingData, 'csessionid') : [];

$allLedgerLines    = getAllLedgerLines($conn, $itineraries, $properties, $owners);
$mergedLedgerLines = array_merge($allLedgerLines['bookingLines'], $allLedgerLines['propertyLines'], $allLedgerLines['ownerLines']);
$ledgerLines       = implode(",", array_column($mergedLedgerLines, '__pk'));

$itineraries = implode(",", $itineraries);
$properties  = implode(",", $properties);

print_r( $allLedgerTaxLinks                = queryAllLedgerTaxLinks($conn, $ledgerLines, $type));
print_r( $allOwnerLedgerLines              = queryOwnerLedgerLines($conn, $ledgerLines, $type));
print_r( $allPaymentAllocationSummaryLines = queryAllPaymentAllocationSummaryLines($conn, $ledgerLines, $type));
print_r( $allItineraryCancellationReasons  = queryAllItineraryCancellationReasons($conn, $itineraries, $type));
print_r( $allItineraryPartyDetails         = queryAllItineraryPartyDetails($conn, $itineraries, $type));
print_r( $allOwnerBookingSettings          = queryAllOwnerBookingSettings($conn, $itineraries, $type));
print_r( $allReletItineraries              = queryAllReletItineraries($conn, $properties, $type));
print_r( $allItineraryNotes                = queryAllItineraryNotes($conn, $itineraries, $type));
print_r( $allPaymentNotes                  = queryAllPaymentNotes($conn, $itineraries, $type));
print_r( $allPaymentDetails                = queryAllPaymentDetails($conn, $itineraries, $type));
print_r( $allPayments                      = queryAllPayments($conn, $itineraries, $type));
print_r( $allAdhocAdjustments              = queryAllAdhocAdjustments($conn, $itineraries, $type));
print_r( $allLedgerSettlementLines         = queryAllLedgerSettlementLines($conn, $ledgerLines, $type));
print_r( $ownerPaymentDetails              = queryOwnerPaymentDetails($conn, $ledgerLines, $type));
print_r( $allLedgerLines                   = queryAllLedgerLines($conn, $ledgerLines, $type));
print_r( $allItineraryLines                = queryAllItineraryLines($conn, $itineraries, $type));
print_r( $allItinerarySettings             = queryAllItinerarySettings($conn, $itineraries, $type));
print_r( $allItineraryOptions              = queryAllItineraryOptions($conn, $itineraries, $type));
print_r( $allStatementSummaryDetails       = queryAllStatementSummaryDetails($conn, $properties, $type));
print_r( $allStatementSummary              = queryAllStatementSummary($conn, $properties, $type));
print_r( $allItineraries                   = queryAllItineraries($conn, $itineraries, $type));
print_r( $allPropertySupplements           = queryAllPropertySupplements($conn, $properties, 'select'));
print_r( $allSupplements                   = queryAllSupplements($conn, implode(",", $allPropertySupplements), 'select'));
print_r( $allSupplementValues              = queryAllSupplementValues($conn, implode(",", $allPropertySupplements), 'select'));
print_r( $allSupplementCurrencyAmounts     = queryAllSupplementCurrencyAmounts($conn, implode(",", $allSupplementValues), 'select'));

queryAllSupplementCurrencyAmounts($conn, implode(",", $allSupplementValues), 'delete');
queryAllSupplementValues($conn, implode(",", $allPropertySupplements), 'delete');
queryAllSupplements($conn, implode(",", $allPropertySupplements), 'delete');
queryAllPropertySupplements($conn, $properties, 'delete');
