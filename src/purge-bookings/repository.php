<?php

function getBookingData(PDO $conn, $brand, $bookings): array
{
    $sql = "SELECT i.nitineraryid, i.csessionid, i.cpbn, i.npersonid, i.npropertyid 
            FROM sykes_reservations.itinerary i 
            JOIN toms.properties p ON p.__pk = i.npropertyid AND p._fk_brand = {$brand};";

    if (! empty($bookings)) {
        $bookings = implode(" ,", $bookings);

        $sql = "SELECT i.nitineraryid, i.csessionid, i.cpbn, i.npersonid, i.npropertyid 
            FROM sykes_reservations.itinerary i 
            JOIN toms.properties p ON p.__pk = i.npropertyid
            JOIN acquisitions.entity_id_mapping AS eim on eim.sykes_entity_id = i.nitineraryid
            WHERE eim.reference_id IN ({$bookings}) AND 
                  eim._fk_brand = {$brand} AND
                  eim.entity_type = 'booking' AND
                  p._fk_brand = {$brand};";
    }

    $result = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    echo 'Fetched Booking Data' . PHP_EOL;;;

    return $result;
}

function getAllLedgerLines(PDO $conn, $bookings, $properties, $owners): array
{
    $itineraryList = implode(' ,', $bookings);
    $propertyList  = implode(' ,', $properties);
    $ownerList     = implode(' ,', $owners);

    $sql = "SELECT nl.__pk FROM sykes_finance.nominal_ledger nl
            WHERE nl._fk_itinerary IN ({$itineraryList});";

    $bookingLines = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    echo 'Fetched Booking Ledger Lines' . PHP_EOL;

    $sql = "SELECT nl.__pk FROM sykes_finance.nominal_ledger nl 
            WHERE nl._fk_property IN ({$propertyList}) AND COALESCE(nl._fk_itinerary, 0) = 0;";

    $propertyLines = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    echo 'Fetched Property Ledger Lines' . PHP_EOL;

    $sql = "SELECT nl.__pk FROM sykes_finance.nominal_ledger nl 
            WHERE nl._fk_owner IN ({$ownerList}) AND COALESCE(nl._fk_property, 0) = 0 AND COALESCE(nl._fk_itinerary, 0) = 0;";

    $ownerLines = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    echo 'Fetched Owner Ledger Lines' . PHP_EOL;

    return [
        'bookingLines'  => $bookingLines,
        'propertyLines' => $propertyLines,
        'ownerLines'    => $ownerLines
    ];
}

function getAllOwners(PDO $conn, $brand): array
{
    $sql    = "SELECT o.__pk FROM toms.owners o WHERE o._fk_brand = {$brand};";
    $result = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    echo 'Fetched All Owners' . PHP_EOL;;

    return $result;
}

function getAllProperties(PDO $conn, $brand): array
{
    $sql    = "SELECT p.__pk FROM toms.properties p WHERE p._fk_brand = {$brand};";
    $result = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    echo 'Fetched All Properties' . PHP_EOL;

    return $result;
}

function queryAllLedgerLines(PDO $conn, $ledgerLines, $type = 'select')
{
    if (empty($ledgerLines)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " nl.* FROM sykes_finance.nominal_ledger nl WHERE nl.__pk IN ({$ledgerLines});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Ledger Lines' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}


function queryAllLedgerSettlementLines(PDO $conn, $ledgerLines, $type = 'select')
{
    if (empty($ledgerLines)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " nls.* FROM sykes_finance.nominal_ledger_settlement nls WHERE nls._fk_nominal_ledger IN ({$ledgerLines});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Ledger Settlement Lines' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryOwnerPaymentDetails(PDO $conn, $ledgerLines, $type = 'select')
{
    if (empty($ledgerLines)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " opd.* FROM sykes_finance.owner_payment_detail opd WHERE opd._fk_nominal_ledger IN ({$ledgerLines});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Owner Payment Detail Lines' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllLedgerTaxLinks(PDO $conn, $ledgerLines, $type = 'select')
{
    if (empty($ledgerLines)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " nltl.* FROM sykes_finance.nominal_ledger_tax_links nltl WHERE nltl.tax_id IN ({$ledgerLines}) OR nltl.original_id IN ({$ledgerLines});";

    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Ledger Tax Links' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryOwnerLedgerLines(PDO $conn, $ledgerLines, $type = 'select')
{
    if (empty($ledgerLines)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " ol.* FROM sykes_finance.owner_ledger ol WHERE ol._fk_nominal_ledger IN ({$ledgerLines});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Owner Ledger Lines' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllPaymentAllocationSummaryLines(PDO $conn, $ledgerLines, $type = 'select')
{
    if (empty($ledgerLines)) {
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " pas.* FROM sykes_finance.payment_allocation_summary pas WHERE pas._fk_nominal_ledger IN ({$ledgerLines});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Payment Allocation Summary Lines' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllItineraryCancellationReasons(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " ics.* FROM sykes_reservations.itinerary_cancellation_reasons ics WHERE ics._fk_itinerary IN ({$itineraries});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Itinerary Cancellation Reasons' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllItineraryPartyDetails(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " ipd.* FROM sykes_reservations.itinerary_party_details ipd WHERE ipd._fk_itinerary IN ({$itineraries});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Itinerary Party Details' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllOwnerBookingSettings(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " obs.* FROM sykes_reservations.owner_booking_settings obs WHERE obs._fk_itinerary IN ({$itineraries});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Owner Booking Settings' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllReletItineraries(PDO $conn, $properties, $type = 'select')
{
    if (empty($properties)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " r.* FROM sykes_finance.relet r WHERE r._fk_property IN ({$properties});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Relet Itinerary Lines' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllItineraries(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " i.* FROM sykes_reservations.itinerary i WHERE i.nitineraryid IN ({$itineraries});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Itineraries ' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllPaymentNotes(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " n.* FROM sykes_reservations.notes n
           JOIN sykes_finance.payment p ON n._fk_payment = p.__pk 
           WHERE p.payment_link_id IN ({$itineraries});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Payments Notes' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllItineraryNotes(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " n.* FROM sykes_reservations.notes n WHERE n.nitineraryid IN ({$itineraries});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Itinerary Notes' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllPaymentDetails(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " pd.* FROM sykes_finance.payment_detail pd
            JOIN sykes_finance.payment p ON p.__pk = pd._fk_payment 
            WHERE p.payment_link_id IN ({$itineraries});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Payment Details' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllPayments(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " p.* FROM sykes_finance.payment p WHERE p.payment_link_id IN ({$itineraries});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Payments' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

// TODO SET FOREIGN_KEY_CHECKS = 0
function queryAllAdhocAdjustments(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " ad.* FROM sykes_finance.adhoc_adjustment ad WHERE ad._fk_itinerary IN ({$itineraries});";
    $conn->query('SET FOREIGN_KEY_CHECKS = 0;');

    $result = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $conn->query('SET FOREIGN_KEY_CHECKS = 1;');

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Adhoc Adjustments' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

// TODO SET FOREIGN_KEY_CHECKS = 0
function queryAllItineraryLines(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " il.* FROM sykes_reservations.itinerary_line il WHERE il._fk_itinerary IN ({$itineraries});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Itinerary Lines' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllItinerarySettings(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " ise.* FROM sykes_reservations.itinerary_settings ise WHERE ise._fk_itinerary IN ({$itineraries});";
    $result    = $conn->query($sql)->fetch();

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Itinerary Settings' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllItineraryOptions(PDO $conn, $itineraries, $type = 'select')
{
    if (empty($itineraries)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " ise.* FROM sykes_reservations.itinerary_options ise WHERE ise._fk_itinerary IN ({$itineraries});";
    $result    = $conn->query($sql)->fetch();

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Itinerary Options' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllStatementSummaryDetails(PDO $conn, $properties, $type = 'select')
{
    if (empty($properties)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " ssd.* FROM sykes_finance.statement_summary_details ssd JOIN sykes_finance.statement_summary ss ON ss.__pk = ssd._fk_statement_summary WHERE ss.properties IN ({$properties});";
    $result    = $conn->query($sql)->fetch();

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Statement Summary Details' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllStatementSummary(PDO $conn, $properties, $type = 'select')
{
    if (empty($properties)){
        return;
    }

    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " ss.* FROM sykes_finance.statement_summary ss WHERE ss.properties IN ({$properties});";
    $result    = $conn->query($sql)->fetch();

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Statement Summary' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllCustomersRestrictions(PDO $conn, $customers, $type = 'select')
{
    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " cr.* FROM sykes_reservations.customer_restrictions cr WHERE cr._fk_person IN ({$customers});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Customer Restrictions' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllCustomerNotes(PDO $conn, $customers, $type = 'select')
{
    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " n.* FROM sykes_reservations.notes n WHERE n.npersonid IN ({$customers});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Customer Notes' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllCustomerMarketingPreferences(PDO $conn, $customers, $type = 'select')
{
    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " cmp.* FROM sykes_reservations.customer_marketing_preferences cmp WHERE cmp._fk_person IN ({$customers});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Customer Marketing Preferences' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function queryAllCustomerSessions(PDO $conn, $customers, $type = 'select')
{
    $queryType = $type === 'count' ? 'select' : $type;
    $sql       = $queryType . " s.* FROM sykes_reservations.session s WHERE s.npersonid IN ({$customers});";
    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Customer Sessions' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : $result;
}

function getNewlyAddedCustomers(PDO $conn, $customers, $brand)
{
    $sql    = "SELECT GROUP_CONCAT(DISTINCT i.npersonid) FROM sykes_reservations.itinerary i 
            JOIN sykes_reservations.person p ON p.npersonid = i.npersonid
            JOIN toms.properties p ON p.__pk = i.npropertyid 
            WHERE p.npersonid IN ({$customers}) AND NOT EXISTS(
                SELECT * FROM sykes_reservations.itinerary i2 
                JOIN toms.properties p ON p.__pk = i2.npropertyid 
                WHERE i2.npropertyid = p.__pk AND
                      i2.npersonid = i.npersonid
                      p._fk_brand <> {$brand}
                )";
    $result = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    echo 'Fetched All Newly Added Customers' . PHP_EOL;

    return $result;
}

function queryAllPropertySupplements(PDO $conn, $properties, $type = 'select')
{
    $queryType = $type === 'count' ? 'select' : $type;
    $sql = $queryType . " sp.* FROM sykes_finance.supplement_property sp WHERE sp._fk_property IN ({$properties});";

    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Property Supplements' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : array_column($result, '_fk_supplement');
}

function queryAllSupplements(PDO $conn, $supplements, $type = 'select'){
    $queryType = $type === 'count' ? 'select' : $type;
    $sql = $queryType . " s.* FROM sykes_finance.supplement s 
            WHERE s.__pk IN ({$supplements});";

    $conn->query('SET FOREIGN_KEY_CHECKS = 0;');

    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $conn->query('SET FOREIGN_KEY_CHECKS = 1;');

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Property Supplements' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : array_column($result, '__pk');
}

function queryAllSupplementValues(PDO $conn, $supplements, $type = 'select')
{
    $queryType = $type === 'count' ? 'select' : $type;
    $sql = $queryType . " sv.* 
             FROM sykes_finance.supplement_values sv 
             WHERE sv._fk_supplement IN ({$supplements});";

    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Property Supplements Values' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : array_column($result, '__pk');
}

function queryAllSupplementCurrencyAmounts(PDO $conn, $supplementValues, $type = 'select')
{
    $queryType = $type === 'count' ? 'select' : $type;
    $sql = $queryType . " sca.*
             FROM sykes_finance.supplement_currency_amounts sca 
             WHERE sca._fk_supplement_values IN ({$supplementValues});";

    $result    = $conn->query($sql)->fetchAll(PDO::FETCH_OBJ);

    $echo = $type === 'count' ? ucwords($type) . 'ed' : ucwords($type);
    $echo = $echo . ' All Property Supplements Currency Amounts' . PHP_EOL;

    echo $echo;

    return $type === 'count' ? count($result) : array_column($result, '__pk');
}

