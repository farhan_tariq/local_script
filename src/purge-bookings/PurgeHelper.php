<?php

function executeDeletionConfirmation($query, $count): void
{
    $env = ! empty($environment) ? $environment : getenv('DB_ENV');

    if (empty($env)) {
        echo 'No environment defined in the .env' . PHP_EOL;
        exit;
    }

    $envString = in_array($env, self::UNSAFE_ENVS) ? "\e[31m" . $env . "\e[0m" : $env;

    if (in_array($env, self::UNSAFE_ENVS)) {
        echo "Are you sure you want to run this on " . $envString . " ? Type 'yes' to continue: ";
        $handle = fopen("php://stdin", "r");
        $line   = trim(fgets($handle));
        if ($line != 'yes') {
            echo "ABORTING!\n";
            exit;
        }

        fclose($handle);
        echo "\n";
        echo "Executing on $envString \n";
    }
}