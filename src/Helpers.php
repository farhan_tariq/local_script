<?php
include __DIR__ . '/../dotenv.php';
(new DotEnv(__DIR__ . '/../.env'))->load();

class Helpers
{
    public const LOCAL        = 'LOCAL';
    public const OPTEST       = 'OPTEST';
    public const STAGING      = 'STAGING';
    public const LIVE_REPLICA = 'LIVE_REPLICA';
    public const LIVE = 'LIVE';

    const UNSAFE_ENVS = [self::STAGING, self::LIVE_REPLICA, self::OPTEST, self::LIVE];

    public static function array_stripos($haystack, $needles): bool
    {
        $needles = (array) $needles;
        foreach ($needles as $needle) {
            if (stripos($haystack, $needle) !== false) {
                return true;
            }
        }

        return false;
    }

    public static function initDB($environment = '', $schema = ''): PDO
    {
        $env = ! empty($environment) ? $environment : getenv('DB_ENV');

        Helpers::scriptExecuteConfirmation($env);;

        $config   = Helpers::getEnvironmentConfig($env);
        $database = ! empty($schema) ? $schema : $config['dbname'];
        $dsn      = 'mysql:host=' . $config['host'] . ';' . 'dbname=' . $database;

        // Create connection
        try {
            $conn = new PDO($dsn, $config['username'], $config['password']);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo 'Connected to ' . $env . ' Database !!' . PHP_EOL;
        } catch (PDOException $e) {
            die('Couldn\'t connect : ' . $e->getMessage());
        }

        return $conn;
    }

    public static function readFromCSV($path, $key = ''): array
    {
        $data    = [];
        $headers = [];
        if (! empty($path)) {
            $stream  = fopen($path, 'rb');
            $headers = fgetcsv($stream);

            while (($row = fgetcsv($stream)) !== false) {
                $row = (object) array_combine($headers, $row);
                if (empty($key)) {
                    $data[] = $row;
                }

                if (! empty($key)) {
                    $data[$row->$key] = $row;
                }
            }
        }

        return [
            'headers' => $headers,
            'data'    => $data
        ];
    }

    private static function scriptExecuteConfirmation($environment): void
    {
        $env = ! empty($environment) ? $environment : getenv('DB_ENV');

        if (empty($env)) {
            echo 'No environment defined in the .env' . PHP_EOL;
            exit;
        }

        $envString = in_array($env, self::UNSAFE_ENVS) ? "\e[31m" . $env . "\e[0m" : $env;

        if (in_array($env, self::UNSAFE_ENVS)) {
            echo "Are you sure you want to run this on " . $envString . " ? Type 'yes' to continue: ";
            $handle = fopen("php://stdin", "r");
            $line   = trim(fgets($handle));
            if ($line != 'yes') {
                echo "ABORTING!\n";
                exit;
            }

            fclose($handle);
            echo "\n";
            echo "Executing on $envString \n";
        }
    }

    private static function getEnvironmentConfig(string $environment): array
    {
        if ($environment === self::LOCAL) {
            return [
                'host'     => getenv('DB_LOCAL_SERVERNAME'),
                'username' => getenv('DB_LOCAL_USERNAME'),
                'password' => getenv('DB_LOCAL_PASSWORD'),
                'dbname'   => getenv('DB_LOCAL_NAME'),
                'sql_mode' => getenv('DB_LOCAL_SQL_MODE'),
            ];
        }

        if ($environment === self::OPTEST) {
            return [
                'host'     => getenv('DB_OPTEST_SERVERNAME'),
                'username' => getenv('DB_OPTEST_USERNAME'),
                'password' => getenv('DB_OPTEST_PASSWORD'),
                'dbname'   => getenv('DB_OPTEST_NAME'),
            ];
        }

        if ($environment === self::STAGING) {
            return [
                'host'     => getenv('DB_STAGING_SERVERNAME'),
                'username' => getenv('DB_STAGING_USERNAME'),
                'password' => getenv('DB_STAGING_PASSWORD'),
                'dbname'   => getenv('DB_STAGING_NAME'),
            ];
        }

        if ($environment === self::LIVE_REPLICA) {
            return [
                'host'     => getenv('DB_LIVE_REPLICA_SERVERNAME'),
                'username' => getenv('DB_LIVE_REPLICA_USERNAME'),
                'password' => getenv('DB_LIVE_REPLICA_PASSWORD'),
                'dbname'   => getenv('DB_LIVE_REPLICA_NAME'),
            ];
        }

        if ($environment === self::LIVE) {
            return [
                'host'     => getenv('DB_LIVE_SERVERNAME'),
                'username' => getenv('DB_LIVE_USERNAME'),
                'password' => getenv('DB_LIVE_PASSWORD'),
                'dbname'   => getenv('DB_LIVE_NAME'),
            ];
        }

        return [];
    }

    public static function createFile(string $filename)
    {
        if (! is_file($filename)) {
            touch($filename);
        }

        echo 'File created: ' . $filename . PHP_EOL;
    }

    public static function openFile(string $filename, $mode)
    {
        $out = fopen($filename, $mode);
        if (! $out) {
            die('Could not open file: "' . $filename . '"');
        }

        return $out;
    }

    public static function validateScriptInput($inputs): void
    {
        foreach ($inputs as $input) {
            if (empty($input)) {
                echo "\033[01;31mPlease input the data dump date and database env \033[0m" . PHP_EOL;
                echo "Example Input \n\033[1;33mphp report.php '2022-01-01' 'LOCAL'\033[0m" . PHP_EOL;
                exit();
            }
        }
    }
}
