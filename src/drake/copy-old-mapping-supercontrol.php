<?php
include_once '../Helpers.php';

/**
 * This script converts file and column mapping of one brand to another. Specific to SuperControl acquisitions
 */

const SCHEMA = 'character_cottages_supercontrol';

$conn                   = Helpers::initDB();
$tableMappings          = [];
$tableCsvColumnMappings = [];

$query = "SELECT * FROM drake.csv_table_mappings ctm WHERE ctm.schema = '" . SCHEMA . "';";

if ($result = $conn->query($query)) {
    while ($obj = $result->fetchObject()) {
        $updatedMapping = str_replace('11679', '3251', $obj->mapping);
        $updatedMapping = str_replace('Character-Cottages', 'Northumbria-Coast-and-Country-Cottages', $updatedMapping);

        $tableMappings[$obj->__pk] = (object) [
            '__pk'            => $obj->__pk,
            'schema'          => $obj->schema,
            'table'           => $obj->table,
            'mapping'         => $updatedMapping,
            'created_date'    => $obj->created_date,
            'last_updated_by' => $obj->last_updated_by,
            'updated_date'    => $obj->updated_date,
        ];
    }
}

$query2 = "SELECT ccm.*,ctm.mapping as file_mapping FROM drake.`csv_table_mappings` ctm 
            JOIN drake.`csv_column_mappings` ccm ON ctm.`__pk` = ccm.`_fk_table_mapping`
          WHERE ctm.`schema` =  '" . SCHEMA . "'
          GROUP BY ccm.`__pk`;";


if ($result2 = $conn->query($query2)) {
    while ($obj2 = $result2->fetchObject()) {
        $updatedMapping = str_replace('11679', '3251', $obj2->file_mapping);
        $updatedMapping = str_replace('Character-Cottages', 'Northumbria-Coast-and-Country-Cottages', $updatedMapping);

        $tableCsvColumnMappings[$obj2->__pk] = (object) [
            '__pk'              => $obj2->__pk,
            '_fk_table_mapping' => $obj2->_fk_table_mapping,
            'schema'            => $obj2->schema,
            'table'             => $obj2->table,
            'column'            => $obj2->column,
            'mapping'           => $obj2->mapping,
            'created_date'      => $obj2->created_date,
            'last_updated_by'   => $obj2->last_updated_by,
            'updated_date'      => $obj2->updated_date,
            'file_mapping'      => $updatedMapping,
        ];
    }
}

$tableMappingsClone          = $tableMappings;
$tableCsvColumnMappingsClone = $tableCsvColumnMappings;

// Get all duplicate mappings for tables to remove
$duplicateTableMappings = getDuplicateMappings($tableMappings);

// remove all duplicate mappings for tables
removeDuplicates($tableMappings, $duplicateTableMappings);

// Get all duplicate column mappings to remove
$duplicateColumnMappings = getDuplicateMappings($tableCsvColumnMappings);

var_export($duplicateColumnMappings);

// remove all duplicate column mappings
//removeDuplicates($tableCsvColumnMappings, $duplicateColumnMappings);

// Output the query to insert table mappings into DB
//var_export(getInsertTableMappingsQuery($tableMappings));

// Run when data has been inserted in the DB to get the inserted tables
//$insertedTableMappings = getInsertedTableMappings($conn);

// Depends on $insertedTableMappings
// Output column mappings query by using old column mappings against the newly inserted table mappings
//$columnMappingsQuery = getInsertColumnMappingsQuery($tableCsvColumnMappings, $insertedTableMappings);

// Output the query to insert column mappings into DB
//var_export($columnMappingsQuery);

function getDuplicateMappings(array $mappings): array
{
    $mappingsClone     = $mappings;
    $duplicateMappings = [];

    foreach ($mappings as $key => $mapping) {
        foreach ($mappingsClone as $key2 => $mappingDup) {
            if (
                $key !== $key2 &&
                $mapping->table === $mappingDup->table &&
                $mapping->mapping === $mappingDup->mapping
            ) {
                $duplicateMappings[$key] = $key2;
            }
        }
    }

    return $duplicateMappings;
}

function removeDuplicates(array &$array, array $duplicates): array
{
    $removed = [];

    if (count($duplicates) > 0) {
        foreach ($duplicates as $dup1 => $dup2) {
            if ($dup1 < $dup2) {
                $removed[] = $array[$dup1];
                unset($array[$dup1]);
            }
        }
    }

    return $removed;
}

function getInsertTableMappingsQuery(array $tableMappings): string
{
    $insertQuery = 'INSERT INTO drake.csv_table_mappings (`schema`,  `table`,  `mapping`,  `created_date`,  `last_updated_by`,  `updated_date`) VALUES ';
    foreach ($tableMappings as $mapping) {
        $insertQuery .= '("' . SCHEMA . '"' . ',"' .
            $mapping->table . '","' .
            $mapping->mapping . '",' .
            'NOW()' . ',' .
            '"farhan.tariq"' . ',' .
            'NOW()' .
            '),';
    }

    return $insertQuery;
}

function getInsertColumnMappingsQuery(array $columnMappings, array $insertedTableMappings): string
{
    $insertQuery = 'INSERT INTO `drake`.`csv_column_mappings` (`_fk_table_mapping`,`schema`,`table`,`column`,`mapping`,`created_date`,`last_updated_by`,`updated_date`) VALUES';

    foreach ($columnMappings as $columnMapping) {
        foreach ($insertedTableMappings as $insertedTableMapping) {
            if ($columnMapping->table === $insertedTableMapping->table && $columnMapping->file_mapping === $insertedTableMapping->mapping) {
                $insertQuery .= '(' . $insertedTableMapping->__pk . ',"' .
                    $insertedTableMapping->schema . '","' .
                    $insertedTableMapping->table . '","' .
                    $columnMapping->column . '","' .
                    $columnMapping->mapping . '",' .
                    'NOW()' . ',' .
                    '"farhan.tariq"' . ',' .
                    'NOW()' .
                    '),';
            }
        }
    }

    return $insertQuery;
}

function getInsertedTableMappings(PDO $conn): array
{
    $query    = 'SELECT * FROM drake.csv_table_mappings ctm WHERE ctm.`schema` = "' . SCHEMA . '"';
    $mappings = [];

    if ($result = $conn->query($query)) {
        while ($obj = $result->fetchObject()) {
            $mappings[$obj->__pk] = $obj;
        }
    }

    return $mappings;
}
