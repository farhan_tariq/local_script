<?php
include '../Helpers.php';

const DATE      = '2022-12-05';
const DIRECTORY = '/home/farhan/Documents/M-A-Dumps/NCCC/';
const BRAND     = 563;

$conn        = Helpers::initDB('LOCAL');
$schema      = getSchemaMapping(BRAND);
$filePrefix  = getFileMapping(BRAND);
$folder      = getFolderMapping(BRAND);
$query       = "SELECT ctm.table, ctm.mapping FROM drake.`csv_table_mappings` ctm WHERE ctm.`schema` = '" . $schema . "';";
$mappings    = $conn->query($query)->fetchAll();
$mappedFiles = [];

foreach ($mappings as $mapping) {
    $mappedFiles[$mapping[0]][] = $mapping[1];
}

$files        = scandir(DIRECTORY . DATE . '/');
$matchedFiles = getMatchedFiles($mappedFiles);

//renameFiles($files, $filePrefix, $folder);
createNewDirectory();
copyFilesToNewFolder($matchedFiles);
printTruncateQueries($matchedFiles, $mappedFiles, $schema);

function getSchemaMapping($brandId): string
{
    switch ($brandId) {
        case 323 :
            return 'character_cottages_supercontrol';

        case 562:
            return 'best_of_suffolk_supercontrol';

        case 563:
            return 'nccc';

        default:
            return '';
    }
}

function getFileMapping($brandId): string
{
    switch ($brandId) {
        case 323 :
            return 'sykes-Character-Cottages-export';

        case 562:
            return 'sykes-Best-of-Suffolk-export';

        case 563:
            return 'sykes-Northumbria-Coast-and-Country-Cottages-export';

        default:
            return '';
    }
}

function getFolderMapping($brandId): int
{
    switch ($brandId) {
        case 323 :
            return 11679;

        case 562:
            return 681;

        case 563:
            return 3251;

        default:
            return -1;
    }
}

function getMatchedFiles($mappedFiles): array
{
    $matchedFiles   = [];
    $directoryFiles = scandir(DIRECTORY . DATE . '/');

//    foreach ($directoryFiles as $key => $file) {
//        $directoryFiles[$key] = str_replace('_.csv', '.csv', $file);
//    }

    foreach ($mappedFiles as $mappedTable) {
        foreach ($mappedTable as $mappedTableFile) {
            if (in_array($mappedTableFile, (array) $directoryFiles, true) && ! in_array($mappedTableFile, $matchedFiles, true)) {
                $matchedFiles[] = $mappedTableFile;
            }
        }
    }

    return $matchedFiles;
}

function renameFiles($files, $filePrefix, $folder): void
{
    foreach ($files as $file) {
        if (in_array($file, ['.', '..'])) {
            continue;
        }

        $fixedName = str_replace('_.csv', '.csv', $file);
        $newName   = DIRECTORY . DATE . '/' . $fixedName;

        if ($fixedName === $filePrefix . '-bookings' . '.csv') {
            $newName = DIRECTORY . DATE . '/' . $folder . '-bookingsOriginal.csv';
        }

        if ($fixedName === $filePrefix . '-booking-finances' . '.csv') {
            $newName = DIRECTORY . DATE . '/' . $folder . '-bookingFinancesOriginal.csv';
        }

        if ($newName !== DIRECTORY . DATE . '/' . $file) {
            rename(
                DIRECTORY . DATE . '/' . $file,
                $newName
            );
        }
    }
}

function createNewDirectory(): void
{
    $directory = DIRECTORY . DATE . '/' . 'mappedFiles';

    if (! is_dir($directory) && ! mkdir($directory) && ! is_dir($directory)) {
        throw new \RuntimeException(sprintf('Directory "%s" was not created', DIRECTORY . DATE . '/' . 'mappedFiles'));
    }

}

function copyFilesToNewFolder($matchedFiles): void
{
    foreach ($matchedFiles as $files) {
        copy(
            DIRECTORY . DATE . '/' . $files,
            DIRECTORY . DATE . '/' . 'mappedFiles/' . $files
        );
    }
}

function printTruncateQueries($matchedFiles, $mappedFiles, $schema): void
{
    $truncateTables = [];

    foreach ($mappedFiles as $table => $mappedTableFiles) {
        foreach ($mappedTableFiles as $tableFile) {
            if (
                in_array($tableFile, $matchedFiles, true) &&
                ! in_array($table, $truncateTables, true)
            ) {
                $truncateTables[] = $table;
            }
        }
    }

    ksort($truncateTables);

    foreach ($truncateTables as $table) {
        echo 'TRUNCATE TABLE ' . $schema . '.' . $table . ';' . PHP_EOL;
    }
}
