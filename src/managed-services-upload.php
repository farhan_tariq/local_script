<?php

include_once 'Helpers.php';

const BRAND                         = 'John Bray';
const IMPORT_DIRECTORY              = __DIR__ . '/../../imports/';
const EXPORT_DIRECTORY              = __DIR__ . '/../../exports/';
const JOHNBRAY_READ_FILE            = IMPORT_DIRECTORY . BRAND . '/JB-managed-services-suppliers.csv';
const JOHNBRAY_FILE_OUT             = EXPORT_DIRECTORY . BRAND . '/JB-managed-services-suppliers-parsed.csv';
const STAGING_ENCRYPT_KEY           = '';
const TEST_ENCRYPT_KEY              = '';
const UK_BANK_SORT_CODE_LENGTH      = 6;
const UK_BANK_ACCOUNT_NUMBER_LENGTH = 8;

$readStream = fopen(JOHNBRAY_READ_FILE, 'rb');
$outStream  = fopen(JOHNBRAY_FILE_OUT, 'wb');
$data       = [];
$codes      = [];
$conn       = Helpers::initDB();

if (! $readStream || ! $outStream) {
    die('Could not open file: "' . ! $readStream ? JOHNBRAY_READ_FILE : JOHNBRAY_FILE_OUT . '"');
}


$headers          = fgetcsv($readStream);
$formattedHeaders = getHeaders();

$originalBankDetails = [];
$fixedBankDetails    = [];

while (($row = fgetcsv($readStream)) !== false) {

    $row = array_combine($headers, $row);

    $originalBankDetails [] = [$row['Sort Code'], $row['Account Number']];

    $row['Sort Code']      = addLeadingZeroes($row['Sort Code'], UK_BANK_SORT_CODE_LENGTH);
    $row['Account Number'] = addLeadingZeroes($row['Account Number'], UK_BANK_ACCOUNT_NUMBER_LENGTH);

    $fixedBankDetails [] = [$row['Sort Code'], $row['Account Number']];

    $data [] = $row;
}


fputcsv($outStream, $headers);

foreach ($data as $datum) {
    $row = array_combine($headers, $datum);
    fputcsv($outStream, $row);
}

fclose($outStream);

function fetchSuppliersByCode(PDO $conn, array $codes): array
{
    $suppliers = [];

    $sql = 'SELECT * FROM purchasing_suppliers ps
            WHERE ps.code IN ("' . implode('","', $codes) . '")';

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            $suppliers[] = $obj;
        }
    }
    return $suppliers;
}

function addAdditionalData(PDO $conn, array $codes, array &$data)
{
    $suppliers = fetchSuppliersByCode($conn, $codes);

    foreach ($suppliers as $supplier) {
        if (! empty($data[$supplier->code])) {
            $data[$supplier->code]['Account name'] = $supplier->payee_name;
            $data[$supplier->code]['Bank']         = $supplier->payee_bank_name;

            preg_match('((?i)Miss|Mrs|Mr|Ms|Dr|Rev|Prof|Lady|Lord|Captain|Mr & Mrs)', $supplier->payee_name, $titleMatches);

            if (! empty($titleMatches)) {
                $data[$supplier->code]['Title'] = $titleMatches[0];
            } else {
                preg_match('((?i)Miss|Mrs|Mr|Ms|Dr|Rev|Prof|Lady|Lord|Captain|Mr & Mrs)', $supplier->payee_bank_name, $titleMatches2);
                if (! empty($titleMatches2)) {
                    $data[$supplier->code]['Title'] = $titleMatches2[0];
                }
            }

            $sortCode      = $data[$supplier->code]['Sort Code'];
            $accountNumber = $data[$supplier->code]['Account Number'];
        }
    }
}

function getHeaders(): array
{
    return [
        'supplier_id'                    => "Supplier ID",
        'supplier_name'                  => "Supplier Name",
        'supplier_type'                  => "Supplier Type",
        'title'                          => "Title",
        'first_name'                     => "First Name",
        'surname'                        => "Surname",
        'company'                        => "Company",
        'url'                            => "URL",
        'currency'                       => "Currency",
        'address_line_1'                 => "Address Line 1",
        'address_line_2'                 => "Address Line 2",
        'address_line_3'                 => "Address Line 3",
        'town'                           => "Town",
        'county'                         => "County",
        'country'                        => "Country",
        'postcode'                       => "Postcode",
        'telephone_1'                    => "Telephone 1",
        'telephone_2'                    => "Telephone 2",
        'telephone_1_–_2'                => "Telephone 1 – 2",
        'email_1'                        => "Email 1",
        'email_2'                        => "Email 2",
        'acquisition_id'                 => "Acquisition ID",
        'brand'                          => "Brand",
        'day_of_payment'                 => "Day of payment",
        'terms'                          => "Terms",
        'vat_status'                     => "VAT Status",
        'vat_number'                     => "VAT Number",
        'company_number'                 => "Company Number",
        'payment_method'                 => "Payment Method",
        'combine_period_/_payment_group' => "Combine Period / Payment Group",
        'bank'                           => "Bank",
        'branch_'                        => "Branch ",
        'account_name'                   => "Account name",
        'sort_code'                      => "Sort Code",
        'account_number'                 => "Account Number",
        'iban'                           => "IBAN",
        'swift_code'                     => "Swift Code"
    ];
}

function toLowerCaseAndUnderscore(array $headers): array
{
    $array = [];
    foreach ($headers as $header) {
        $formatted = strtolower($header);
        $formatted = str_replace(' ', '_', $formatted);
        $array[]   = $formatted;
    }

    return $array;
}

function sortData(array &$data): void
{
    usort($data, 'compare');
}

function compare(array $a, array $b): int
{
    return strcmp($a['Supplier ID'], $b['Supplier ID']);
}


function addLeadingZeroes(string $string, int $count): string
{
    $length = strlen($string);
    $temp   = '';

    if ($length != $count) {
        $diff = $count - $length;

        for ($i = 0; $i < $diff; $i++) {
            $temp .= '0';
        }
    }

    return $temp . $string;
}
