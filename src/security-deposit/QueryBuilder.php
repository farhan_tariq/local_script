<?php

/**
 * Parses a file with no headers and with just 2 columns (_fk_itinerary, amount) and
 * outputs either insert or update queries for security deposits.
 */
class QueryBuilder
{
    private array $csv;

    public function __construct($csvPath)
    {
        $this->csv = $this->readFromCsv($csvPath);
    }

    public function getInsertQueryStrings(): array
    {
        $settledInsertQuery = 'INSERT INTO sykes_reservations.itinerary_settled_outside_system (_fk_itinerary, amount) VALUES ';
        $noteInsertQuery    = 'INSERT INTO sykes_reservations.notes (cnote, nitineraryid, nloginid, tcreated) VALUES ';

        foreach ($this->csv as $line) {
            [
                $itineraryId,
                $amount,
            ] = $line;

            $settledInsertQuery .= "($itineraryId, $amount),";
            $noteInsertQuery    .= "('We have received a £$amount security deposit which is due to be paid back to the customer 7 days after the holiday end date', $itineraryId, 237, NOW()),";
        }

        return [
            'insertSettledQuery' => rtrim($settledInsertQuery, ",") . ";\n\n",
            'notesInsertQuery'   => rtrim($noteInsertQuery, ",") . ";\n\n",
        ];
    }

    public function getUpdateQueryStrings(): array
    {
        $settledUpdateQuery = '';
        $noteUpdateQuery    = 'INSERT INTO sykes_reservations.notes (cnote, nitineraryid, nloginid, tcreated) VALUES ';

        foreach ($this->csv as $line) {
            [
                $itineraryId,
                $amount,
            ] = $line;

            $settledUpdateQuery .= "UPDATE  `sykes_reservations`.`itinerary_settled_outside_system` SET  `amount` = $amount WHERE `_fk_itinerary` = $itineraryId;";
            $noteUpdateQuery    .= "('We have received a £$amount security deposit which is due to be paid back to the customer 7 days after the holiday end date', $itineraryId, 237, NOW()),";

        }

        return [
            'updateSettledQuery' => rtrim($settledUpdateQuery, ",") . ";\n\n",
            'notesUpdateQuery'   => rtrim($noteUpdateQuery, ",") . ";\n\n"
        ];
    }

    private function readFromCsv(string $path): array
    {
        if (empty($path)) {
            throw new ErrorException('Path cannot be empty');
        }

        return array_map('str_getcsv', file($path));
    }

    public function writeToFile(string $outputFileName, string $directory, array $data): void
    {
        if (! is_dir($directory)) {
            mkdir($directory);
        }

        if (! is_file($outputFileName)) {
            touch($outputFileName);
        }

        $out = fopen($outputFileName, 'wb');

        if (! $out) {
            die('Could not open file: "' . $outputFileName . '"');
        }

        foreach ($data as $datum) {
            fwrite($out, $datum);
        }

        fclose($out);
    }
}
