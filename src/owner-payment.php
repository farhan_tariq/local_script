<?php
include "Helpers.php";

$conn = Helpers::initDB('OPTEST');

// HOTL arrears terms owner payment
$conn->query("SET @sproc_run_date='2022-12-13 15:12:34'");
$result = $conn->query('CALL sykes_finance.sp_eventMakeOwnerPayment(56, NULL, 194, 1);');
var_export([
    'HOTL Term',
    $result->fetchObject()
]);

// CASH terms owner payment
//$conn->query("SET @sproc_run_date='2023-05-02'");
//$result = $conn->query('CALL sykes_finance.sp_eventMakeOwnerPayment(24, NULL, 194, 1);');
//var_export([
//    'Cash Term',
//    $result->fetchObject()
//    ]);
//
//// Sykes Arrears terms payment
//$conn->query("SET @sproc_run_date='2023-05-02'");
//$result = $conn->query('CALL sykes_finance.sp_eventMakeOwnerPayment(22, NULL, 194, 1);');
//var_export([
//    'Arrears Term',
//    $result->fetchObject()
//]);