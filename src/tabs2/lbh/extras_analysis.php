<?php

use Carbon\Carbon;

include_once '../Helpers.php';

$conn = Helpers::initDB();

$query = 'SELECT * FROM bookings b 
JOIN booking_items bi ON bi.booking = b.__pk = bi.booking_id
WHERE bi.extra_type IN()';


function getStatus($booking)
{
    $todayDate      = (new Carbon())->toDateTimeString();
    $blankDate      = '1970-01-01 00:00:00';
    $expiryDateTime = $booking->hold_expiry_date;

    //OWNER BOOKING LOGIC
    if ($booking->booking_type === 'Agency' || $booking->booking_type === 'Owner') {
        if ($booking->cancelled_date !== $blankDate) {
            return 'CANCELLED_OWNER_BOOKING';
        }

        return 'OWNER_BOOKING';
    }

    $bookingPayments = getPayments($booking);
    $depositAmount   = getDepositAmount($booking);
    $paid            = $bookingPayments->sum('amount');

    //Cancelled Bookings
    if ($booking->cancelled_date !== $blankDate) {
        if (! $bookingPayments->isEmpty()) {
            return 'CANCELLED_CONFIRMED';
        }

        if ($booking->transferred_date !== $blankDate && ($booking->deposit_paid_date !== $blankDate)) {
            return 'CANCELLED_CONFIRMED';
        }

        return 'CANCELLED_HELD';
    }

    //Non Transferred Bookings && paid anything on the booking.
    if (
        $depositAmount > 0 && $paid > 0 &&
        $booking->cancelled_date == $blankDate &&
        $booking->transferred_date === $blankDate
    ) {
        return 'CONFIRMED';
    }

    //Non Transferred Bookings && paid anything on the booking but have been cancelled.
    if (
        $depositAmount > 0 && $paid > 0 &&
        $booking->cancelled_date !== $blankDate &&
        $booking->transferred_date === $blankDate
    ) {
        return 'CANCELLED_CONFIRMED';
    }

    //Transferred Bookings that have been confirmed then transferred.
    if (
        $booking->confirmed_date !== $blankDate &&
        $booking->deposit_paid_date !== $blankDate &&
        $booking->transferred_date !== $blankDate
    ) {
        return 'CANCELLED_CONFIRMED';
    }

    //Confirmed non transferred booking (Dont think i need this, too scared to remove).
    if (
        $booking->confirmed_date !== $blankDate &&
        $booking->deposit_paid_date !== $blankDate &&
        $booking->transferred_date === $blankDate
    ) {
        return 'CONFIRMED';
    }

    //Catch the odd transferred bookings that are confirmed with no payments.
    if ($booking->transferred_date !== $blankDate && $paid == 0) {
        return 'CANCELLED_HELD';
    }

    if ($booking->confirmed_date !== $blankDate) {
        return 'CONFIRMED';
    }

    //Provisional (held) Logic
    if ($booking->hold_expired === 1 || ($expiryDateTime !== $blankDate && $todayDate >= $expiryDateTime)) {
        return 'CANCELLED_HELD';
    }

    if (
        $booking->hold_expired === 0 &&
        $booking->deposit_paid_date === $blankDate &&
        $booking->confirmed_date == $blankDate
    ) {
        return 'HELD';
    }

    return 'CANCELLED_HELD';
}