<?php
include_once '../src/Helpers.php';

const FILENAME         = 'expected-values-2022-09-14-Dump.csv';
const BRAND_NAME       = 'bos';
const IMPORT_DIRECTORY = __DIR__ . '/../imports/' . BRAND_NAME . '/expected_values/';
const SCHEMA           = 'bos_import_data';

$conn = Helpers::initDB('OPTEST');

$bookingCount          = 'bookingid';
$debtorsControlAccount = 'Debtors';
$debtors2              = 'Debtors 2';
$sykesDeferred         = 'Sykes Deferred';
$ownerDeferred         = 'Owner Deferred';
$ownerBalances         = 'Owner Balances';
$bankCurrentAccount    = 'Bank';
$sykesRecIncome        = 'Sykes Income';

$filteredBookings = fopen(IMPORT_DIRECTORY . FILENAME, 'rb');

if (! $filteredBookings) {
    die('Could not open file: "' . IMPORT_DIRECTORY . FILENAME . '"');
}

$headers = fgetcsv($filteredBookings);
$sql     = "INSERT INTO " . SCHEMA . ".expected_values 
    (`booking_ref`, `debtors`, `debtors2`, `bank`, `sykes_rec_income`, `sykes_def`, `owner_def`, `owner_balances`) VALUES ";

while (($row = fgetcsv($filteredBookings)) !== false) {
    $row = (object) array_combine($headers, $row);

    // SQL to use with Jon SpreadSheet data
    $sql .= "('{$row->$bookingCount}',{$row->$debtorsControlAccount},{$row->$debtors2},{$row->$bankCurrentAccount},{$row->$sykesRecIncome},{$row->$sykesDeferred},{$row->$ownerDeferred},{$row->$ownerBalances}),";
}

$sql = substr($sql, 0, -1);

$conn->query("TRUNCATE TABLE  " . SCHEMA . ".expected_values;");
$conn->query($sql);
