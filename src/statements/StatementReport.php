<?php

class StatementReport
{
    protected PDO $connection;
    protected string $folderPath;
    protected string $brandName;
    protected string $brandId;
    protected string $ownerPaymentId;
    protected string $env;

    /**
     * @param PDO $connection
     * @param string $folderPath
     * @param string $brandName
     * @param string $brandId
     * @param string $env
     */
    public function __construct(PDO $connection, string $folderPath, string $brandName, string $brandId, string $ownerPaymentId, string $env)
    {
        $this->connection     = $connection;
        $this->folderPath     = $folderPath;
        $this->brandName      = $brandName;
        $this->brandId        = $brandId;
        $this->ownerPaymentId = $ownerPaymentId;
        $this->env            = $env;
    }

    public function process(): void
    {
        $statementSummary = $this->getStatementSummary();
        $statementDetails = $this->getStatementDetails();
        $environment      = in_array($this->env, [Helpers::LIVE, Helpers::LIVE_REPLICA]) ? 'live' : strtolower($this->env);

        $this->writeToCsv($statementSummary, 'statement-summary-' . $this->brandName . '-' . $this->ownerPaymentId . '-' . $environment);
        $this->writeToCsv($statementDetails, 'statement-details-' . $this->brandName . '-' . $this->ownerPaymentId . '-' . $environment);
    }

    public function getStatementSummary(): array
    {
        $sql = "SELECT ss.__pk,
                       ss._fk_owner,
                       ss._fk_owner_payment_id,
                       ss.properties                             AS properties,
                       ss.due_from_booking                       AS booking,
                       ss.fees_and_charges                       AS fees_and_charges,
                       ss.payments                               AS payments,
                       ss.due_from_booking + ss.fees_and_charges AS Net,
                       ss.balance_forward                        AS b_fwd,
                       ss.total                                  AS total,
                       CASE
                           p.live_status
                           WHEN 'N'
                               THEN 'New'
                           WHEN 'C'
                               THEN 'Current'
                           WHEN 'G'
                               THEN 'Going'
                           WHEN 'L'
                               THEN 'Lapsed'
                           END                                   AS property_live_status,
                       p.test_property                           AS is_test_property,
                       eim_owner.reference_id                    AS acquisition_owner_id
                FROM sykes_finance.statement_summary ss
                         JOIN toms.owners o ON ss._fk_owner = o.__pk
                         JOIN toms.properties p ON ss.properties = p.__pk
                         JOIN acquisitions.`entity_id_mapping` eim_owner ON eim_owner.sykes_entity_id = o.__pk AND eim_owner.entity_type = 'owner' AND eim_owner._fk_brand = {$this->brandId}
                WHERE ss._fk_owner_payment_id IN ({$this->ownerPaymentId})
                  AND ss.active = 1
                  AND p._fk_brand = {$this->brandId}
                GROUP BY ss.__pk
                ORDER BY p._fk_owner;";

        return $this->connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getStatementDetails(): array
    {
        $sql = "SELECT ss.__pk,
                       i.tcreated                                                         AS 'itinerary_created_date',
                       CONCAT(COALESCE(o.name_first, ''), ' ', COALESCE(o.name_last, '')) AS 'owner_name',
                       ssd.__pk                                                           AS statement_detail_id,
                       _fk_statement_summary,
                       ssd._fk_owner_payment_id,
                       ssd._fk_owner,
                       nitineraryid                                                       AS '_fk_itinerary',
                       ssd._fk_property,
                       ssd._fk_owner_payment_group,
                       ssd._fk_line_type,
                       ssd.owner_invoice_id,
                       ssd.type,
                       i.cpbn                                                             AS 'booking_reference',
                       CONCAT(COALESCE(p.cforename, ''), ' ', COALESCE(p.cforename, ''))  AS 'customer_name',
                       i.tstart                                                           AS 'itinerary_start_date',
                       i.tend                                                             AS 'itinerary_end_date',
                       lo.spare_3                                                         AS 'booking_status',
                       rental_amount,
                       extras,
                       received_from_customer,
                       relet_change,
                       relet_vat,
                       commission_change,
                       commission_vat,
                       total_commission,
                       commission_vat                                                     AS 'commission_vat_2',
                       total_due_to_owner,
                       previously_paid,
                       future_amount,
                       settlement,
                       is_new,
                       is_imported,
                       created_at,
                       eim.reference_id                                                                                                                                                                          AS 'acquisition_booking_ref',
                       lo.spare_3                                                                                                                                                                                AS 'booking_status',
                       ct._fk_itinerary                                                                                                                                                                          AS 'in_covid_tracking_table',
                       ct.refund_cohort                                                                                                                                                                          AS 'refund_cohort',
                       ss.balance_forward,
                       IF(ssd.owner_invoice_id IS NOT NULL AND ssd.owner_invoice_id <> 0, (SELECT nl.`desc` FROM sykes_finance.nominal_ledger nl WHERE ssd.owner_invoice_id = nl.owner_invoice_id), (SELECT '')) AS 'desc',
                       IF(ssd.owner_invoice_id IS NOT NULL AND ssd.owner_invoice_id <> 0, (SELECT nl.`ref` FROM sykes_finance.nominal_ledger nl WHERE ssd.owner_invoice_id = nl.owner_invoice_id), (SELECT ''))  AS 'ref',
                       IF(ssd.owner_invoice_id IS NOT NULL AND ssd.owner_invoice_id <> 0, (SELECT nl.`due_date` FROM sykes_finance.nominal_ledger nl WHERE ssd.owner_invoice_id = nl.owner_invoice_id), (SELECT ''))  AS 'invoice_date',
                       eim_owner.reference_id                                                                                                                                                                    AS 'acquisition_owner_ref'
                FROM sykes_finance.statement_summary ss
                         LEFT JOIN sykes_finance.statement_summary_details ssd ON ssd._fk_statement_summary = ss.__pk
                         JOIN      toms.owners o ON ss._fk_owner = o.__pk
                #        LEFT JOIN sykes_finance.nominal_ledger nl ON ssd.owner_invoice_id = nl.owner_invoice_id AND ssd.owner_invoice_id is not null and ssd.owner_invoice_id <> 0
                         LEFT JOIN sykes_reservations.itinerary i ON ssd._fk_itinerary = i.nitineraryid
                         LEFT JOIN toms.properties prop ON prop.__pk IN (ssd._fk_property)
                         LEFT JOIN sykes_reservations.person p ON p.npersonid = i.npersonid
                         LEFT JOIN sykes_reservations.covid_tracking ct ON i.nitineraryid = ct._fk_itinerary
                         LEFT JOIN sykes_platform.lookup_options lo ON lo.__pk = i.cstatus
                         LEFT JOIN acquisitions.entity_id_mapping eim ON eim.sykes_entity_id = i.nitineraryid AND eim.entity_type = 'booking' AND eim._fk_brand = {$this->brandId}
                         LEFT JOIN acquisitions.`entity_id_mapping` eim_owner ON eim_owner.sykes_entity_id = prop._fk_owner AND eim_owner.entity_type = 'owner' AND eim_owner._fk_brand = {$this->brandId}
                         LEFT JOIN toms.properties p ON i.npropertyid = p.__pk
                WHERE ss._fk_owner_payment_id IN ({$this->ownerPaymentId})
                  AND ss.active = 1
                  AND prop._fk_brand = {$this->brandId};";

        return $this->connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function writeToCsv(array $data, $filename): void
    {
        if (empty($data)){
            return;
        }

        if (! file_exists($this->folderPath)) {
            mkdir($this->folderPath, 0777, true);
        }

        $writer = fopen($this->folderPath . '/' . $filename . '.csv', 'wb');

        $headers = array_keys($data[0]);

        fputcsv($writer, $headers);

        foreach ($data as $row) {
            $row = array_combine($headers, (array) $row);
            fputcsv($writer, $row);
        }

        fclose($writer);

        echo 'Data written to CSV file: ' . $this->folderPath . '/' . $filename . '.csv' . PHP_EOL;
    }
}