<?php
include_once "../Helpers.php";
include_once "../statements/StatementReport.php";

const EXPORT_DIRECTORY = __DIR__ . '/../../exports';
const BRAND_ID         = '563';
const BRAND_NAME       = 'nccc';
const SCHEMA           = 'sykes_finance';
const OWNER_PAYMENT_ID = 2460;
const ENV              = 'OPTEST';

$conn = Helpers::initDB(ENV, SCHEMA);

$report = new StatementReport(
    $conn,
    EXPORT_DIRECTORY . '/' . BRAND_NAME . '/' . 'statements',
    BRAND_NAME,
    BRAND_ID,
    OWNER_PAYMENT_ID,
    ENV
);

$report->process();
