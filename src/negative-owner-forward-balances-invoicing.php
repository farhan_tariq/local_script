<?php
include "Helpers.php";

/**
 * Adds invoices for negative brought forward balances. Only handles owners having a single property
 */

const EXPORT_DIRECTORY           = __DIR__ . '/../exports/';
const BRAND_DIRECTORY            = 'nccc/';
const BRAND_ID                   = 563;
const BRAND_NAME                 = 'nccc';
const DUMP_DATE                  = '2022-12-05';
const SETTLEMENT_QUERY_DIRECTORY = EXPORT_DIRECTORY . BRAND_DIRECTORY . DUMP_DATE . '/settlement-queries/';

const ACCOUNT_BALANCE_RECLAIM_ACCOUNT = 3522;
const DESCRIPTION                     = "'Balance brought forward'";
const NOMINAL_ACCOUNT_TYPE_ID         = ACCOUNT_BALANCE_RECLAIM_ACCOUNT;
const ENTITY_OWNER                    = 1;
const ENTITY_OFFICE                   = 4;
const ADD_VAT_ROW                     = 0;

$settlementOwners = [
    38085  => -40.00,
    173800 => -40.50,
    152362 => -48.00,
    173617 => -48.00,
    164930 => -48.00,
    159641 => -48.00,
    169501 => -48.00,
    170200 => -48.00,
    175602 => -48.00,
    151164 => -48.00,
    174331 => -48.00,
    162051 => -48.00,
    167810 => -48.00,
    161216 => -48.00,
    149686 => -48.00,
    156619 => -48.00,
    147766 => -56.95,
    35620  => -78.83,
    170201 => -96.00,
    35521  => -96.00,
    143345 => -98.00,
    150482 => -102.52,
    155460 => -110.00,
    153174 => -111.00,
    35682  => -127.85,
    105842 => -131.36,
    158197 => -134.50,
    152031 => -166.00,
    151437 => -185.94,
    37771  => -201.60,
    174596 => -203.50,
    171318 => -209.50,
    35472  => -220.69,
    35402  => -235.00,
    151749 => -265.29,
    161228 => -293.71,
    172807 => -309.00,
    41538  => -320.83,
    159613 => -358.43,
    35483  => -376.32,
    151888 => -397.70,
    39348  => -416.80,
    38368  => -512.60,
    152248 => -518.50,
    163268 => -561.99,
    150099 => -773.00,
    43613  => -877.50,
];

$outputFileName = SETTLEMENT_QUERY_DIRECTORY . "owner-settlement-queries-" . DUMP_DATE . '-Dump-' . strtoupper(BRAND_NAME) . '.sql';

if (! is_dir(SETTLEMENT_QUERY_DIRECTORY)) {
    mkdir(SETTLEMENT_QUERY_DIRECTORY);
}

$db = Helpers::initDB('LIVE');

$settlePks = [];
$failed    = [];
$owners    = [];

foreach ($settlementOwners as $acquisitionOwnerId => $value) {
    $enterpriseOwnerId    = getEnterpriseOwnerId($db, $acquisitionOwnerId);

    if (empty($enterpriseOwnerId)){
        echo "Acquisition Owner: {$acquisitionOwnerId} not imported to Enterprise" . PHP_EOL;
        continue;
    }

    $enterprisePropertyId = getEnterpriseOwnerPropertyId($db, $enterpriseOwnerId);

    if (empty($enterprisePropertyId)) {
        echo "Acquisition Owner: {$acquisitionOwnerId} has no property imported in Enterprise" . PHP_EOL;
        continue;
    }

    $owners[$enterpriseOwnerId] = [
        'description'          => DESCRIPTION,
        'nominalAccountId'     => NOMINAL_ACCOUNT_TYPE_ID,
        'itineraryId'          => 'NULL',
        'enterpriseOwnerId'    => $enterpriseOwnerId,
        'amount'               => $value,
        'entity'               => ENTITY_OFFICE,
        'enterprisePropertyId' => $enterprisePropertyId,
        'supplierId'           => 'NULL',
        'addVatRow'            => ADD_VAT_ROW,
    ];
}

if (! empty($owners)) {
    Helpers::createFile($outputFileName);
    $settlementQueryOut = Helpers::openFile($outputFileName, 'wb');

    foreach ($owners as $enterpriseOwnerId => $value) {
        $queries = dropOwnerAdhocInvoicesTable() . PHP_EOL;
        $queries .= createOwnerAdhocInvoicesTable() . PHP_EOL;
        $queries .= insertIntoAdhocInvoicesTable($value) . PHP_EOL;
        $queries .= (int) $enterpriseOwnerId !== 434019 ? "SET @sproc_run_date = '2022-11-07 13:00:00';" . PHP_EOL : "SET @sproc_run_date = '2022-11-14 13:00:00';" . PHP_EOL;

        $queries .= callSproc($enterpriseOwnerId) . PHP_EOL;

        fwrite($settlementQueryOut, $queries);
        echo $queries;
    }

    fwrite($settlementQueryOut, dropOwnerAdhocInvoicesTable());
    echo dropOwnerAdhocInvoicesTable();

    fclose($settlementQueryOut);
}

function getEnterpriseOwnerId($conn, $acquisitionOwnerId)
{
    $query = "SELECT * FROM acquisitions.entity_id_mapping eim 
               WHERE eim._fk_brand = " . BRAND_ID . " AND 
                     eim.entity_type = 'owner' AND 
                     eim.reference_id = {$acquisitionOwnerId} ;";

    $result = $conn->query($query)->fetchObject();

    if ($result){
        return $result->sykes_entity_id;
    }

    return null;
}

function getEnterpriseOwnerPropertyId($conn, $enterpriseOwnerId)
{
    $query = "SELECT p.__pk FROM toms.properties p 
              WHERE p._fk_owner = {$enterpriseOwnerId} AND 
                    p.live_status IN ('C', 'G', 'L') 
              LIMIT 1;";

    $result = $conn->query($query)->fetchObject();

    if (! empty($result)) {
        return $result->__pk;
    }

    return null;
}

function createOwnerAdhocInvoicesTable(): string
{
    return 'CREATE TEMPORARY TABLE IF NOT EXISTS sykes_finance.tmp_ownerAdhocInvoices(`__pk` INT PRIMARY KEY AUTO_INCREMENT,`description` VARCHAR(255),`nominal_account_type_id` INT UNSIGNED, `_fk_itinerary` INT UNSIGNED,`_fk_owner` INT UNSIGNED,`amount_owner_settlement` DECIMAL(10, 2),`_fk_entity` INT(11) UNSIGNED,`_fk_property` INT(11) UNSIGNED,`_fk_supplier` INT UNSIGNED,`add_vat_row` TINYINT(1) UNSIGNED);';
}

function dropOwnerAdhocInvoicesTable(): string
{
    return 'DROP TEMPORARY TABLE IF EXISTS sykes_finance.tmp_ownerAdhocInvoices;';
}

function insertIntoAdhocInvoicesTable($data): string
{
    return "INSERT INTO sykes_finance.tmp_ownerAdhocInvoices(`description`, `nominal_account_type_id`, `_fk_itinerary`, `_fk_owner`, `amount_owner_settlement`, `_fk_entity`, `_fk_property`, `_fk_supplier`, `add_vat_row`) VALUES ({$data['description']}, {$data['nominalAccountId']}, {$data['itineraryId']}, {$data['enterpriseOwnerId']}, {$data['amount']}, {$data['entity']}, {$data['enterprisePropertyId']}, {$data['supplierId']}, {$data['addVatRow']});";
}

function callSproc($enterpriseOwnerId)
{
    return "CALL sykes_finance.sp_eventAdhocOwnerInvoice({$enterpriseOwnerId});";
}
