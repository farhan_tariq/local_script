<?php
include_once __DIR__ . '/../src/security-deposit/QueryBuilder.php';

const BRAND_NAME   = 'nccc';
const GO_LIVE_DATE = '2022-12-05';

$isInsert = true;
$fileType = $isInsert ? 'insert' : 'update';

$importDirectory = __DIR__ . '/../imports/' . BRAND_NAME . '/' . GO_LIVE_DATE . '/' . 'security-deposits/';
$exportDirectory = __DIR__ . '/../exports/' . BRAND_NAME . '/' . GO_LIVE_DATE . '/' . 'security-deposit-queries/';
$importFilename  = $importDirectory . "sd-import-" . GO_LIVE_DATE . ".csv";
$exportFilename  = $exportDirectory . "sd-import-{$fileType}queries-" . GO_LIVE_DATE . ".sql";

$securityDepositBuilder = new QueryBuilder($importFilename);

if ($isInsert) {
    $insertStrings = $securityDepositBuilder->getInsertQueryStrings();
    echo $insertStrings['insertSettledQuery'] . PHP_EOL;
    echo $insertStrings['notesInsertQuery'];

    $securityDepositBuilder->writeToFile(
        $exportFilename,
        $exportDirectory,
        [
            $insertStrings['insertSettledQuery'],
            $insertStrings['notesInsertQuery']
        ]
    );
}

if (! $isInsert) {
    $updateStrings = $securityDepositBuilder->getUpdateQueryStrings();
    echo $updateStrings['updateSettledQuery'] . PHP_EOL;
    echo $updateStrings['notesUpdateQuery'];

    $securityDepositBuilder->writeToFile(
        $exportFilename,
        $exportDirectory,
        [
            $updateStrings['updateSettledQuery'],
            $updateStrings['notesUpdateQuery']
        ]
    );
}
