<?php

use Carbon\Carbon;

require __DIR__ . '/../vendor/autoload.php';

const CONFIRMED_FILE = __DIR__ . '/../imports/CCH/cch-filtered-bookings.14thDec-Go-Live-dump-Confirmed.csv';
const CANCELLED_FILE = __DIR__ . '/../imports/CCH/cch-filtered-bookings.14thDec-Go-Live-dump-Cancelled.csv';

$testProperties = [
    517160, // Test Property
    521100, // Test Property
    587840,  // Test Property
];

$interestedBookings = [1032251, 1034326, 1031994, 1033889, 1021585, 1032227, 1023727, 1031389, 1032655, 1021933, 1026231, 1026180, 1032248, 1033318, 1032790, 1032708, 1033913, 1032747, 1033548, 1038481, 1034990, 1038084, 1035745, 1038201, 1039450, 1038601, 1039342, 1036461, 1039305, 1035471, 1039110, 1038019, 1036200, 1038696, 1037877, 1039676, 1040627, 1039640, 1040153, 1039669, 1039592, 1041394, 1040804, 1041267, 1039944, 1040317, 1040035, 1040987, 1042536, 1042270, 1042584, 1042188, 1042126, 1043008, 1043522, 1043904, 1043170, 1043202, 1043874, 1043867, 1043850, 1043389, 1042868, 1044068, 1043984, 1044885, 1044715, 1044046, 1044817, 1043965, 1044308, 1044075, 1045497, 1045183, 1045626, 1045273, 1032854, 1041190, 1041952,];
$importThenSettle   = [1031389, 1032655, 1026231, 1032248, 1033318, 1032708, 1033548, 1034990, 1038601, 1036461, 1038019, 1037877, 1039676, 1041267, 1040035, 1042584, 1042188, 1043202, 1042868, 1044715,];

$confirmedData     = Helpers::readFromCSV(CONFIRMED_FILE);
$cancelledData     = Helpers::readFromCSV(CANCELLED_FILE);
$confirmedBookings = $confirmedData['data'];
$cancelledBookings = $cancelledData['data'];

$reletBookings = [];

foreach ($cancelledBookings as $key => $cancelledBooking) {
    if (in_array((int) $cancelledBooking->{'Property ref'}, $testProperties) ||
        $cancelledBooking->{'Booking Type'} === 'OWNER' ||
        ! in_array((int) $cancelledBooking->{'Booking Ref'}, $importThenSettle)
    ) {
        continue;
    }

    echo $key . PHP_EOL;

    $startA = new Carbon($cancelledBooking->{'Holiday start date'});
    $endA   = new Carbon($cancelledBooking->{'Holiday end date'});

    foreach ($confirmedBookings as $confirmedBooking) {
        if ($confirmedBooking->{'Booking Type'} === 'OWNER') {
            continue;
        }

        $startB = new Carbon($confirmedBooking->{'Holiday start date'});
        $endB   = new Carbon($confirmedBooking->{'Holiday end date'});

        if ($cancelledBooking->{'Property ref'} === $confirmedBooking->{'Property ref'} && $cancelledBooking->{'Booking Ref'} !== $confirmedBooking->{'Booking Ref'}) {

            if ($startA->lt($endA) && $startB->lt($endB) && $startA->lt($endB) && $endA->gt($startB)) {
                $reletBookings[] = $cancelledBooking->{'Booking Ref'} . ' - ' . $confirmedBooking->{'Booking Ref'};
            }
        }
    }
}

var_export($reletBookings);
