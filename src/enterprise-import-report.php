<?php
include __DIR__ . "/Helpers.php";

define('DUMP_DATE', ! empty($argv[1]) ? $argv[1] : null);
define('ENV', ! empty($argv[2]) ? $argv[2] : null);

Helpers::validateScriptInput([DUMP_DATE, ENV]);

const BRAND            = 563;
const BRAND_NAME       = 'nccc';
const SCHEMA           = 'nccc_import_data';
const EXPORT_DIRECTORY = __DIR__ . '/../exports';
const PRE_SETTLEMENT   = true;

$conn = Helpers::initDB(ENV);
$url  = getUrl(ENV);

// Truncate Table
$conn->query(getTruncateQuery(SCHEMA));

// Insert Actuals
$conn->query(getInsertActualsQuery(SCHEMA, BRAND));

generateCSV($conn, getSykesDeferredQuery(SCHEMA, BRAND, $url), 'sykes-deferred');
generateCSV($conn, getOwnerDeferredQuery(SCHEMA, BRAND, $url), 'owner-deferred');
generateCSV($conn, getOwnerDepositsQuery(SCHEMA, BRAND, $url), 'owner-deposits');
generateCSV($conn, getSykesIncomeQuery(SCHEMA, BRAND, $url), 'sykes-income');
generateCSV($conn, getDebtorsQuery(SCHEMA, BRAND, $url), 'debtors');
generateCSV($conn, getBankQuery(SCHEMA, BRAND, $url), 'bank');
generateCSV($conn, getAllAccounts(BRAND, $url), PRE_SETTLEMENT ? 'account-level-info-pre-settlement' : 'account-level-info-post-settlement');
generateCSV($conn, getInsertedValues(BRAND, $url), 'actual-values');

function getTruncateQuery(string $schema): string
{
    return 'TRUNCATE TABLE ' . $schema . '.' . '`actual_values`';
}

function getInsertActualsQuery(string $schema, int $brand): string
{
    return "
        INSERT INTO " . $schema . ".`actual_values` (
          `booking_ref`,
          `sykes_booking_ref`,
          `debtors`,
          `debtors2`,
          `bank`,
          `sykes_rec_income`,
          `sykes_def`,
          `owner_def`,
          `owner_balances`
        )
        SELECT
          eim.reference_id,
          eim.sykes_entity_id,
          SUM(IF(nl.nominal_account_type_id = 129, nl.amount_sterling, 0)) AS 'Debtors Control Account',
          SUM(IF(nl.nominal_account_type_id = 3371, nl.amount_sterling, 0)) AS 'Debtors 2',
          SUM(IF(nl.nominal_account_type_id = 133, nl.amount_sterling, 0)) AS 'Bank Current Account',
          SUM(IF(nl.nominal_account_type_id IN ( 142, 145, 160, 164, 165, 205, 211, 220, 4239), nl.amount_sterling, 0)) AS 'Sykes Income',
          SUM(IF(nl.nominal_account_type_id = 130, nl.amount_sterling, 0)) AS 'Sykes Deferred',
          SUM(IF(nl.nominal_account_type_id = 131, nl.amount_sterling, 0)) AS 'Owner Deferred',
          SUM(IF(nl.nominal_account_type_id = 139, nl.amount_sterling, 0)) AS 'Owner Balances - Deposits'
        FROM
          acquisitions.`entity_id_mapping`    AS eim
          JOIN sykes_reservations.`itinerary` AS i  ON i.`nitineraryid` = eim.`sykes_entity_id`
          JOIN sykes_finance.`nominal_ledger` AS nl ON nl.`_fk_itinerary` = i.`nitineraryid`
        WHERE eim.`entity_type` = 'booking'
          AND eim.`_fk_brand` = $brand
        GROUP BY i.nitineraryid
        LIMIT 999999;";
}

function getSykesDeferredQuery(string $schema, int $brand, string $url): string
{
    return "
        SELECT
         'Sykes def differences',
          CONCAT('{$url}', eim.sykes_entity_id) AS url,
          eim.sykes_entity_id,
          av.`booking_ref`,
          ev.`sykes_def`                    AS 'expected',
          av.`sykes_def`                    AS 'actual',
          (av.`sykes_def` - ev.`sykes_def`) AS 'difference'
        FROM
          " . $schema . ".`actual_values` AS av
          JOIN  " . $schema . ".`expected_values`  AS ev   ON av.`booking_ref` = ev.`booking_ref`
          JOIN acquisitions.entity_id_mapping          AS eim  ON eim.reference_id = ev.booking_ref
            AND eim._fk_brand = $brand
            AND eim.entity_type = 'booking'
          LEFT JOIN  " . $schema . ".`known_diffs` AS kd   ON kd.`booking_ref` = av.`booking_ref`
        WHERE ev.`sykes_def` <> av.`sykes_def`
          AND kd.`booking_ref` IS NULL
        HAVING difference > 0.5 OR  difference < -0.5;";
}

function getOwnerDeferredQuery(string $schema, int $brand, string $url): string
{
    return "
         SELECT
         'Owner def differences',
          CONCAT('{$url}',eim.sykes_entity_id) AS url,
          eim.sykes_entity_id,
          av.`booking_ref`,
          ev.`owner_def`                    AS 'expected',
          av.`owner_def`                    AS 'actual',
          (av.`owner_def` - ev.`owner_def`) AS 'difference'
        FROM
           " . $schema . ".`actual_values`        AS av
          JOIN  " . $schema . ".`expected_values` AS ev
            ON av.`booking_ref` = ev.`booking_ref`
          JOIN acquisitions.entity_id_mapping AS eim  ON eim.reference_id = ev.booking_ref
            AND eim._fk_brand = $brand
            AND eim.entity_type = 'booking'
          LEFT JOIN  " . $schema . ".`known_diffs` AS kd ON kd.`booking_ref` = av.`booking_ref`
        WHERE ev.`owner_def` <> av.`owner_def`
          AND kd.`booking_ref` IS NULL
        HAVING difference > 0.5 OR  difference < -0.5;";
}

function getSykesIncomeQuery(string $schema, int $brand, string $url): string
{
    return "
        SELECT
         'Sykes rec income',
          CONCAT('{$url}',eim.sykes_entity_id) AS url,
          eim.sykes_entity_id,
          av.`booking_ref`,
          ev.`sykes_rec_income`                           AS 'expected',
          av.`sykes_rec_income`                           AS 'actual',
          (av.`sykes_rec_income` - ev.`sykes_rec_income`) AS 'difference'
        FROM
           " . $schema . ".`actual_values` AS av
          JOIN  " . $schema . ".`expected_values` AS ev  ON av.`booking_ref` = ev.`booking_ref`
          JOIN acquisitions.entity_id_mapping AS eim         ON eim.reference_id = ev.booking_ref
            AND eim._fk_brand = $brand
            AND eim.entity_type = 'booking'
          LEFT JOIN  " . $schema . ".`known_diffs` AS kd ON kd.`booking_ref` = av.`booking_ref`
        WHERE ev.`sykes_rec_income` <> av.`sykes_rec_income`
        AND kd.`booking_ref` IS NULL
        HAVING difference > 0.5 OR  difference < -0.5;";
}

function getOwnerDepositsQuery(string $schema, int $brand, string $url): string
{
    return " 
        SELECT
         'Owner balances differences',
          CONCAT('{$url}',eim.sykes_entity_id) AS url,
          eim.sykes_entity_id,
          av.`booking_ref`,
          ev.`owner_balances`                         AS 'expected',
          av.`owner_balances`                         AS 'actual',
          (av.`owner_balances` - ev.`owner_balances`) AS 'difference'
        FROM
           " . $schema . ".`actual_values` AS av
          JOIN  " . $schema . ".`expected_values` AS ev  ON av.`booking_ref` = ev.`booking_ref`
          JOIN acquisitions.entity_id_mapping         AS eim ON eim.reference_id = ev.booking_ref
            AND eim._fk_brand = $brand
            AND eim.entity_type = 'booking'
          LEFT JOIN  " . $schema . ".`known_diffs` AS kd ON kd.`booking_ref` = av.`booking_ref`
        WHERE ev.`owner_balances` <> av.`owner_balances`
          AND kd.`booking_ref` IS NULL
        HAVING difference > 0.5 OR  difference < -0.5;";
}

function getDebtorsQuery(string $schema, int $brand, string $url): string
{
    return " 
        SELECT
         'debtors differences',
          CONCAT('{$url}', eim.sykes_entity_id) AS url,
          eim.sykes_entity_id,
          av.`booking_ref`,
          ev.`debtors`                  AS 'expected',
          av.`debtors`                  AS 'actual',
          (av.`debtors` - ev.`debtors`) AS 'difference'
        FROM
           " . $schema . ".`actual_values` AS av
          JOIN  " . $schema . ".`expected_values` AS ev  ON av.`booking_ref` = ev.`booking_ref`
          JOIN acquisitions.entity_id_mapping         AS eim ON eim.reference_id = ev.booking_ref
            AND eim._fk_brand = $brand
            AND eim.entity_type = 'booking'
          LEFT JOIN  " . $schema . ".`known_diffs` AS kd ON kd.`booking_ref` = av.`booking_ref`
        WHERE ev.debtors <> av.debtors
          AND kd.`booking_ref` IS NULL
        HAVING difference > 0.5 OR  difference < -0.5;";
}

function getBankQuery(string $schema, int $brand, string $url): string
{
    return "
    SELECT
         'bank differences',
          CONCAT('{$url}', eim.sykes_entity_id) AS url,
          eim.sykes_entity_id,
          av.`booking_ref`,
          ev.`bank`                  AS 'expected',
          av.`bank`                  AS 'actual',
          (av.`bank` - ev.`bank`) AS 'difference'
        FROM
           " . $schema . ".`actual_values` AS av
          JOIN  " . $schema . ".`expected_values` AS ev  ON av.`booking_ref` = ev.`booking_ref`
          JOIN acquisitions.entity_id_mapping         AS eim ON eim.reference_id = ev.booking_ref
            AND eim._fk_brand = $brand
            AND eim.entity_type = 'booking'
          LEFT JOIN  " . $schema . ".`known_diffs` AS kd ON kd.`booking_ref` = av.`booking_ref`
        WHERE ev.bank <> av.bank
            AND kd.`booking_ref` IS NULL
        HAVING difference > 0.5 OR  difference < -0.5;";
}

function getInsertedValues(int $brand, string $url): string
{
    return "SELECT
          eim.reference_id as aquisition_id,
          eim.sykes_entity_id as sykes_entity_id,
          CONCAT('{$url}', eim.sykes_entity_id) AS url,
          SUM(IF(nl.nominal_account_type_id = 129, nl.amount_sterling, 0)) AS 'Debtors Control Account',
          SUM(IF(nl.nominal_account_type_id = 3371, nl.amount_sterling, 0)) AS 'Debtors 2',
          SUM(IF(nl.nominal_account_type_id = 133, nl.amount_sterling, 0)) AS 'Bank Current Account',
          SUM(IF(nl.nominal_account_type_id IN (142, 145, 160, 164, 205, 211, 220, 4239), nl.amount_sterling, 0)) AS 'Sykes Income',
          SUM(IF(nl.nominal_account_type_id = 130, nl.amount_sterling, 0)) AS 'Sykes Deferred',
          SUM(IF(nl.nominal_account_type_id = 131, nl.amount_sterling, 0)) AS 'Owner Deferred',
          SUM(IF(nl.nominal_account_type_id = 139, nl.amount_sterling, 0)) AS 'Owner Balances - Deposits'
        FROM
          acquisitions.`entity_id_mapping`    AS eim
          JOIN sykes_reservations.`itinerary` AS i  ON i.`nitineraryid` = eim.`sykes_entity_id`
          JOIN sykes_finance.`nominal_ledger` AS nl ON nl.`_fk_itinerary` = i.`nitineraryid`
        WHERE eim.`entity_type` = 'booking'
          AND eim.`_fk_brand` = $brand
        GROUP BY i.nitineraryid
        LIMIT 999999;";
}

function getAllAccounts(int $brand, string $url): string
{
    return "
        SELECT
          eim.sykes_entity_id     AS sykes_entity_id,
          eim.`reference_id` AS aquisition_id,
          CONCAT('{$url}',eim.sykes_entity_id) AS url,
          SUM(IF(nominal_account_type_id = 129, nl.amount_sterling, 0  )) AS debtors_control_account,
          SUM(IF(nominal_account_type_id = 3371, nl.amount_sterling, 0 )) AS debters_2,
          SUM(IF(nominal_account_type_id = 133, nl.amount_sterling, 0  )) AS bank_current_account, 
          SUM(IF(nominal_account_type_id IN (142, 145, 160, 164, 165, 205, 211, 220, 4239),nl.amount_sterling, 0)) AS sykes_income_including_supplier_commission,
          SUM(IF(nominal_account_type_id = 130, nl.amount_sterling, 0  )) AS sykes_deferred,
          SUM(IF(nominal_account_type_id = 131, nl.amount_sterling, 0  )) AS owner_deferred,
          SUM(IF(nominal_account_type_id = 139, nl.amount_sterling, 0  )) AS owner_balances_deposits,
          SUM(IF(nominal_account_type_id IN (142, 145, 160, 164, 165, 205, 211, 220, 4239) AND nl.nominal_account_code <> 6592 ,nl.amount_sterling, 0)) AS pure_sykes_income,
          SUM(IF(nominal_account_type_id IN (142, 145, 160, 164, 165, 205, 211, 220, 4239) AND nl.nominal_account_code = 6592 ,nl.amount_sterling, 0)) AS suppliers_commission,
          SUM(IF(nominal_account_type_id = 142, nl.amount_sterling, 0  )) AS sales_tax_control_account,
          SUM(IF(nominal_account_type_id = 145, nl.amount_sterling, 0  )) AS commission, 
          SUM(IF(nominal_account_type_id = 160, nl.amount_sterling, 0  )) AS booking_fee,  
          SUM(IF(nominal_account_type_id = 165, nl.amount_sterling, 0  )) AS relet_income,  
          SUM(IF(_fk_line_type = 8,             nl.amount_sterling, 0  )) AS sales_ledger,  
          SUM(IF(nominal_account_type_id = 205, nl.amount_sterling, 0  )) AS brand_fee,
          SUM(IF(nominal_account_type_id = 220, nl.amount_sterling, 0  )) AS adhoc_adjustments,  
          SUM(IF(nominal_account_type_id = 211, nl.amount_sterling, 0  )) AS supplements_income,  
          SUM(IF(nominal_account_type_id = 4239, nl.amount_sterling, 0  )) AS marketing_uplift,  
          SUM(sykes_finance.`fn_getInvoiceAmountSettled`(nl.__pk)) AS settled,
          DATE(i.`tcreated`) AS booking_created_date,
          CASE   
              WHEN i.cstatus = 79 THEN 'HELD'
              WHEN i.cstatus = 81 THEN 'CONFIRMED'
              WHEN i.cstatus = 116 THEN 'CANCELLED_CONFIRMED'
              WHEN i.cstatus = 199 THEN 'OWNER_BOOKING'
              WHEN i.cstatus = 200 THEN 'CANCELLED_OWNER_BOOKING'
              WHEN i.cstatus = 201 THEN 'OWNER_BOOKING_WITHDRAWING'
              WHEN i.cstatus = 202 THEN 'CANCELLED_HELD'
              WHEN i.cstatus = 217 THEN 'HELD_EXTENDED'
              WHEN i.cstatus = 224 THEN '224'
          ELSE 'UNKOWN'
          END AS booking_status,
            IF(is2.`_fk_deposit_profile` = 36, 'Airbnb', '0% profile') AS deposit_profile,
            IF(s.nsrccodeid = 15453, 'NCCC', IF(s.nsrccodeid = 12020, 'AIRBNB_15%', IF(s.nsrccodeid = 15108, 'AIRBNB_18%' ,'NCCC'))) as booking_source,
          o.`_fk_brand`,
          eim_owner.`acquisition_id` AS owner_ref
     FROM acquisitions.`entity_id_mapping` eim
         JOIN      sykes_reservations.`itinerary` i ON i.`nitineraryid` = eim.sykes_entity_id
         JOIN      sykes_reservations.session s ON s.`csessionid` = i.`csessionid`
         JOIN      sykes_reservations.`itinerary_settings` is2 ON is2.`_fk_itinerary` = i.nitineraryid
         JOIN      toms.`properties` pr ON pr.__pk = i.npropertyid
         JOIN      toms.`owners` o ON o.`__pk` = pr._fk_owner
         JOIN      sykes_platform.`brands` b ON b.`__pk` = o.`_fk_brand`
         JOIN      acquisitions.`entity_id_mapping` eim_owner ON eim_owner.`sykes_entity_id` = o.__pk AND eim_owner.`entity_type` = 'owner'
         LEFT JOIN sykes_finance.nominal_ledger nl ON eim.`sykes_entity_id` = nl.`_fk_itinerary`
         LEFT JOIN sykes_finance.`rfx_credit_settings` cs ON cs.`_fk_deposit_profile` = is2.`_fk_deposit_profile` AND cs.`_fk_rfx` = s.`nsrccodeid`
    WHERE eim._fk_brand = {$brand}
      AND eim.entity_type = 'booking'
      AND o._fk_brand = {$brand}
    GROUP BY i.nitineraryid
    LIMIT 9999999999;";
}

function generateCSV(PDO $conn, string $sql, string $fileName)
{
    if ($result = $conn->query($sql)) {
        $directory = EXPORT_DIRECTORY . '/' . BRAND_NAME . '/' . DUMP_DATE . '/enterprise-import-reports';

        if (! file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        $out      = fopen($directory . '/' . $fileName . '-' . DUMP_DATE . '-Dump-' . strtoupper(BRAND_NAME) . '.csv', 'wb');
        $inserted = false;
        $headers  = [];

        while ($obj = $result->fetchObject()) {
            if (! $inserted) {
                $array   = get_object_vars($obj);
                $headers = array_keys($array);
                fputcsv($out, $headers);
                $inserted = true;
            }

            $row = array_combine($headers, (array) $obj);
            fputcsv($out, $row);
        }
    }
}

function getUrl(string $environment): string
{
    $env = ! empty($environment) ? $environment : getenv('DB_ENV');

    switch ($env) {
        case Helpers::STAGING:
            return 'https://enterprise.staging.sykes.network/itinerary/details/id/';
        case Helpers::OPTEST:
            return 'https://sykes.livetest.office/itinerary/details/id/';
        case Helpers::LIVE:
        case Helpers::LIVE_REPLICA:
            return 'https://enterprise.sykes.network/itinerary/details/id/';
        default:
            return '';
    }
}