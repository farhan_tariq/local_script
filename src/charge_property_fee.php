<?php
include "Helpers.php";

$conn = Helpers::initDB('OPTEST');

$sql        = "SELECT p.__pk FROM toms.properties p WHERE p._fk_brand = 563;";
$properties = array_column($conn->query($sql)->fetchAll(), '__pk');


foreach ($properties as $property) {
    if ($property == 11222071) {
        continue;
    }

    $conn->query("SET @sproc_run_date='2022-12-01 00:00:00';");
    $conn->query("CALL sykes_finance.sp_eventAddReRegFee({$property});");

    $conn->query("SET @sproc_run_date='2023-01-01 00:00:00';");
    $conn->query("CALL sykes_finance.sp_eventAddReRegFee({$property});");

    $conn->query("SET @sproc_run_date='2023-02-01 00:00:00';");
    $conn->query("CALL sykes_finance.sp_eventAddReRegFee({$property});");

    $conn->query("SET @sproc_run_date='2023-03-01 00:00:00';");
    $conn->query("CALL sykes_finance.sp_eventAddReRegFee({$property});");

    $conn->query("SET @sproc_run_date='2023-04-01 00:00:00';");
    $conn->query("CALL sykes_finance.sp_eventAddReRegFee({$property});");

    $conn->query("SET @sproc_run_date='2023-05-01 00:00:00';");
    $conn->query("CALL sykes_finance.sp_eventAddReRegFee({$property});");
}