<?php
require './vendor/autoload.php';

include_once '../Helpers.php';

use Carbon\Carbon;

const BRAND              = 'aqh';
const IMPORT_DIRECTORY   = __DIR__ . '/../../imports/';
const EXPORT_DIRECTORY   = __DIR__ . '/../../exports/';
const ABERSOCH_READ_FILE = IMPORT_DIRECTORY . BRAND . '/In&Outs/1.Aqh IN Oct 20 - Sept 21(Oct).csv';
define("ABERSOCH_FILE_OUT", EXPORT_DIRECTORY . BRAND . '/1.Aqh IN Oct 20 - Sept 21(Oct).' . date('Y_m_d-H') . '.csv');

$spDataReadStream = fopen(ABERSOCH_READ_FILE, 'rb');
$csvOutStream     = fopen(ABERSOCH_FILE_OUT, 'wb');
$data             = [];

if (! $spDataReadStream || ! $csvOutStream) {
    die('Could not open file: "' . ! $spDataReadStream ? ABERSOCH_READ_FILE : ABERSOCH_FILE_OUT . '"');
}

$headers = fgetcsv($spDataReadStream);
while (($row = fgetcsv($spDataReadStream)) !== false) {
    $row = (object) array_combine($headers, $row);


//    $parsedDates = parseDate($row->dates,trim($row->date));
//
//    $row->date_from = $parsedDates[1];
//    $row->date_to = $parsedDates[0];
//    $row->date = $parsedDates[2];
//
    $row->property_name = '';
    $data []            = $row;
}

//array_push($headers,'start_date','end_date','customer_name');
array_push($headers, 'customer_name', 'property_name');

fputcsv($csvOutStream, $headers);

foreach ($data as $row) {
    $splitPropertyName = explode('-', $row->property_original_name);

    if (count($splitPropertyName) > 1) {
        $row->property_name = trim($splitPropertyName[0]);
    }

    $array = [
        $row->fixed_date,
        $row->date_original,
        $row->property_original_name,
        $row->type,
        $row->received,
        $row->owners_old,
        $row->owners_new,
        $row->start_date,
        $row->end_date,
        $row->sec_dep,
        $row->aqh_gross,
        $row->aqh_net,
        $row->vat,
        $row->refund,
        $row->other,
        $row->reason,
        $row->random_column,
        count($splitPropertyName) > 1 ? trim($splitPropertyName[1]) : '',
        count($splitPropertyName) > 1 ? trim($splitPropertyName[0]) : trim($row->property_name),
    ];

    fputcsv($csvOutStream, array_combine($headers, $array));
}

fclose($csvOutStream);


function parseDate($travelDates, $bookingDate): array
{
    $parsedBookingDate = $bookingDate;

    if ($travelDates !== '-' && ! in_array($bookingDate, ['', '#REF!'])) {
        $splitted = explode('-', $travelDates);

        if ((count($splitted) < 2)) {
            $splitted = explode('&', $travelDates);
        }
        $parsedBookingDate = Carbon::parse($bookingDate);

        if (! empty($splitted) && count($splitted) === 2) {
            if (str_contains($splitted[1], '/') && str_contains(strtolower($splitted[1]), 'covid')) {
                $splitted[1] = str_replace('covid', '', strtolower($splitted[1]));
            }

            if (str_contains($splitted[1], '/') && str_contains(strtolower($splitted[1]), 'canc')) {
                $splitted[1] = str_replace('canc', '', strtolower($splitted[1]));
            }

            $regex1 = '((^\d\d|\d)/(\d[0-2]|\d|0\d)/\d{2}$)';
            $regex2 = '((^\d\d|\d)/((\d[0-2])|(\d)|0\d)$)';
            $regex3 = '(\b\d{1,2}\D{0,3})?(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?)';

            preg_match('(' . $regex1 . '|' . $regex2 . '|' . $regex3 . ')', str_replace(' ', '', $splitted[1]), $matches);

            if (! empty($matches)) {
                $isWithYear  = count(explode('/', $splitted[1])) > 2;
                $isSeperated = str_contains($matches[0], '/');
                $endDate     = ! $isWithYear ? $matches[0] . '/' . $parsedBookingDate->year : $matches[0];

                if ($isSeperated) {
                    $formattedEndDate = ! $isWithYear ? Carbon::createFromFormat('d/m/Y', $endDate) : Carbon::createFromFormat('d/m/y', $endDate);
                } else {
                    $endDate          = $matches[0] . ' ' . $parsedBookingDate->year;
                    $formattedEndDate = Carbon::parse($endDate);
                }

                $isStartDateSplit    = str_contains($splitted[0], '/');
                $isStartDateWithYear = count(explode('/', $splitted[0])) > 2;

                if ($isStartDateWithYear) {
                    $startDate = $splitted[0];
                } else {
                    $startDate = ! $isStartDateSplit ? $splitted[0] . '/' . $formattedEndDate->month . '/' . $parsedBookingDate->year : $splitted[0] . '/' . $parsedBookingDate->year;
                }

                $formattedStartDate = Carbon::createFromFormat('d/m/Y', str_replace(' ', '', $startDate));

                if ($formattedStartDate->month == 12 && $formattedEndDate->month > 0 && $formattedEndDate->month != 12) {
                    $formattedEndDate->setYear($formattedStartDate->year + 1);
                }

                if ($formattedEndDate->year != $formattedStartDate->year && $formattedStartDate->year != 12) {
                    $formattedStartDate->setYear($formattedEndDate->year);
                }

                return [$formattedEndDate->format('d-m-Y'), $formattedStartDate->format('d-m-Y'), $parsedBookingDate->format('d-m-Y')];
            }
        }
    }
    return ['', '', is_object($parsedBookingDate) ? $parsedBookingDate->format('d-m-Y') : $bookingDate];
}
