<?php
include_once '../Helpers.php';

const IMPORT_DIRECTORY = __DIR__ . '/../../imports/';
const SUPER_CONTROL_EXPECTED = IMPORT_DIRECTORY . 'CCH/owner-commission-list-2021-07-28-1700.csv';
const BROUGHT_FORWARD_MONTH = 6;
const BROUGHT_FORWARD_DAY = 30;

const DUE_MONTH = 7;
const DUE_END_DAY = 31;

$conn = Helpers::initDB();
$scExpected = [];

$spData = fopen(SUPER_CONTROL_EXPECTED, 'rb');

if (!$spData) {
    die('Could not open file: "' . SUPER_CONTROL_EXPECTED . '"');
}

$headers = fgetcsv($spData);
while (($row = fgetcsv($spData)) !== false) {
    $row = (object)array_combine($headers, $row);
    if (!empty($scExpected[$row->ManagerID])) {
        $scExpected[$row->ManagerID] += $row->Outstanding;
    }

    if (empty($scExpected[$row->ManagerID])) {
        $scExpected[$row->ManagerID] = $row->Outstanding;
    }
}

$broughtForwardBalances = getBroughtForwardBalanceByMonth($conn, BROUGHT_FORWARD_MONTH, BROUGHT_FORWARD_DAY);
$dueByMonthPerDump      = getDueToOwnerByMonthPerDump($conn, DUE_MONTH, DUE_END_DAY);
$reversalDueThisMonth   = getReversalAndCompensationPaymentsDueSameMonth($conn, DUE_MONTH, DUE_END_DAY);

// Take away any brought forward balances
foreach ($broughtForwardBalances as $ownerId => $amount) {
    if (!empty($dueByMonthPerDump[$ownerId])) {
        $dueByMonthPerDump[$ownerId] -= $amount < 0 ? $amount * -1 : $amount;
    }
}

// Take away any reversals or compensations
foreach ($reversalDueThisMonth as $ownerId => $amount) {
    if (!empty($dueByMonthPerDump[$ownerId])) {
        $dueByMonthPerDump[$ownerId] -= $amount < 0 ? $amount * -1 : $amount;
        $dueByMonthPerDump[$ownerId] = round($dueByMonthPerDump[$ownerId], 2);
    }
}

$dumpExpected = $dueByMonthPerDump;

$diff = [];

foreach ($dumpExpected as $ownerId => $outstanding) {
    if (!empty($scExpected[$ownerId])) {
        $totalSum = round($scExpected[$ownerId] - $outstanding, 2);
        if ($totalSum < 0 || $totalSum > 0) {
            $diff[$ownerId] = round($scExpected[$ownerId] - $outstanding, 2);
        }
    }
}

printDiff($diff);
printDueToOwner($dueByMonthPerDump);
printExpectedVSActual($diff, $scExpected, $dumpExpected);


function getBroughtForwardBalanceByMonth($conn, $month, $day): array
{
    $sql = "SELECT opo.owner_id, COALESCE(SUM(opo.amount), 0) AS amount FROM character_cottages_supercontrol.owner_payment_owe opo
            WHERE (opo.`paid_date` IS NULL OR opo.`paid_date` = '0000-00-00 00:00:00')
            AND opo.`payment_date` >= '2021-$month-01' AND opo.`payment_date` <= '2021-$month-$day'
            GROUP BY opo.owner_id;";

    $broughtForwardBalances = [];

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            $broughtForwardBalances[$obj->owner_id] = $obj->amount;
        }
    }

    return $broughtForwardBalances;
}

function getDueToOwnerByMonthPerDump($conn, $month, $day): array
{
    $sql = "SELECT p.`_fk_owner`, SUM(bf.`owner_due`) AS due_to_owner
            FROM character_cottages_supercontrol.booking b
                     JOIN character_cottages_supercontrol.booking_finances bf ON b.`booking_id` = bf.`owner_bookingID`
                     JOIN character_cottages_supercontrol.property p ON p.`__pk` = b.`_fk_property`
            WHERE bf.`invoice_date` >= '2021-$month-01'
              AND bf.`invoice_date` <= '2021-$month-$day'
            GROUP BY p._fk_owner";

    $dueByMonth = [];

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            $dueByMonth[$obj->_fk_owner] = $obj->due_to_owner;
        }
    }
    return $dueByMonth;
}

function getReversalAndCompensationPaymentsDueSameMonth($conn, $month, $day): array
{
    $sql = "SELECT opo.owner_id, COALESCE(SUM(opo.amount), 0) AS amount FROM owner_payment_owe opo
            WHERE (opo.`paid_date` IS NULL OR opo.`paid_date` = '0000-00-00 00:00:00')
            AND opo.`payment_date` >= '2021-$month-01' AND opo.`payment_date` <= '2021-$month-$day'
            AND opo.booking_id = 0
            GROUP BY opo.owner_id;";

    $reversalDueThisMonth = [];

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            $reversalDueThisMonth[$obj->owner_id] = $obj->amount;
        }
    }
    return $reversalDueThisMonth;
}

function printDueToOwner($dueByMonth)
{
    echo "Due to the Owners" . PHP_EOL;
    foreach ($dueByMonth as $owner => $amount) {
        print_r($owner . ' => ' . $amount * 1);
        echo PHP_EOL;
    }

    echo "-----------------------------------------------" . PHP_EOL;
}

function printExpectedVSActual($diff, $expectedPerSC, $actualPerData)
{
    $scExpectedTotal = 0;
    $dumpExpectedTotal = 0;

    echo "Expected VS Actual" . PHP_EOL;

    foreach ($diff as $ownerId => $amount) {
        print_r([$ownerId => ['actualPerData' => $actualPerData[$ownerId], 'expectedPerSC' => $expectedPerSC[$ownerId]]]);
        echo PHP_EOL;
    }

    echo "-----------------------------------------------" . PHP_EOL;

    foreach ($actualPerData as $ownerId => $outstanding) {
        $scExpectedTotal += $expectedPerSC[$ownerId];
        $dumpExpectedTotal += $outstanding;
    }

    var_export([
        'scExpected' => round($scExpectedTotal, 2),
        'dumpExpected' => round($dumpExpectedTotal, 2),
        'Difference' => round($scExpectedTotal - $dumpExpectedTotal, 2),
        'ElementDiff' => count($actualPerData) - count($expectedPerSC)
    ]);
}

function printDiff($diff)
{
    echo "All Differences" . PHP_EOL;
    var_export([
            $diff,
            count($diff)
        ]
    );

    echo PHP_EOL;
    echo "-----------------------------------------------" . PHP_EOL;
}
