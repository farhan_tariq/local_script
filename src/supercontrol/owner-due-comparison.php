<?php
include_once '../Helpers.php';

const BRAND                  = 'CCH';
const IMPORT_DIRECTORY       = __DIR__ . '/../../imports/';
const EXPORT_DIRECTORY       = __DIR__ . '/../../exports/';
const SUPER_CONTROL_EXPECTED = IMPORT_DIRECTORY . 'CCH/owner-commission-list-2021-07-28-1700.csv';
const FOLDER                 = EXPORT_DIRECTORY . BRAND . '/statement-reports/';
const HEADERS                = [
    'Owner ID',
    'SC Owner Due',
    'Dump Owner Due',
    'Diff',
    '',
    'Invoices Raised',
    'Paid against Invoices'
];

$conn = Helpers::initDB();
$scExpected = [];
$fileName  = 'owner-liability-diff.' . BRAND . "." . date('Y-m-d__H:i:s') . '.csv';

$spDataReadStream             = fopen(SUPER_CONTROL_EXPECTED, 'rb');
$ownerLiabilityDiffOutStream  = fopen(FOLDER . $fileName, 'wb');


if (!$spDataReadStream || !$ownerLiabilityDiffOutStream ) {
    die('Could not open file: "' . !$spDataReadStream ? SUPER_CONTROL_EXPECTED : FOLDER . $fileName . '"');
}

$headers = fgetcsv($spDataReadStream);
while (($row = fgetcsv($spDataReadStream)) !== false) {
    $row = (object)array_combine($headers, $row);
    if (!empty($scExpected[$row->ManagerID])) {
        $scExpected[$row->ManagerID] += $row->Outstanding;
    }

    if (empty($scExpected[$row->ManagerID])) {
        $scExpected[$row->ManagerID] = $row->Outstanding;
    }
}

fputcsv($ownerLiabilityDiffOutStream, HEADERS);

$allRaisedInvoices = getAllInvoicesRaisedForOwners($conn);
$allOwnerPayments  = getAllOwnerPayments($conn);
$ownerBreakDown    = getAllOwners($conn);

foreach ($allOwnerPayments as $ownerId => $ownerPayments) {
    foreach ($ownerPayments as $ownerPayment) {
        if (
            (int)$ownerPayment->owner_bookingID > 0 &&
            !empty($allRaisedInvoices[$ownerPayment->owner_bookingID]) &&
            (int)$allRaisedInvoices[$ownerPayment->owner_bookingID]->_fk_owner != (int)$ownerPayment->owner_id
        ) {
            foreach ($allRaisedInvoices as $bookingId => $raisedInvoice) {
                if ($bookingId == $ownerPayment->owner_bookingID) {
                    if ($ownerPayment->is_adjustment){
                        $raisedInvoice->owner_due += $ownerPayment->amount;
                    }
                    $raisedInvoice->_fk_owner = $ownerPayment->owner_id;
                    break;
                }
            }
        }
    }
}

foreach ($allOwnerPayments as $ownerId => $ownerPayments) {
    foreach ($ownerPayments as $ownerPayment) {
        if (!empty($ownerBreakDown[$ownerPayment->owner_id])) {
            $ownerBreakDown[$ownerPayment->owner_id]->owner_payments_total += $ownerPayment->amount;
            $ownerBreakDown[$ownerPayment->owner_id]->owner_payments_total = round($ownerBreakDown[$ownerPayment->owner_id]->owner_payments_total, 2);
        }
    }
}

foreach ($allRaisedInvoices as $bookingId => $raisedInvoice) {
    $ownerBreakDown[$raisedInvoice->_fk_owner]->invoices_raised_total += $raisedInvoice->owner_due;
    $ownerBreakDown[$raisedInvoice->_fk_owner]->invoices_raised_total = round($ownerBreakDown[$raisedInvoice->_fk_owner]->invoices_raised_total, 2);
}

foreach ($ownerBreakDown as $ownerId => $data) {
    if (!empty($scExpected[$ownerId])) {
        $data->scExpected = $scExpected[$ownerId];
        $data->diff = round($scExpected[$ownerId] - round(($data->invoices_raised_total - $data->owner_payments_total), 2), 2);
    } else {
        $data->scExpected = 0;
        $data->diff = round(0 - round(($data->invoices_raised_total - $data->owner_payments_total), 2), 2);
    }
}

foreach ($ownerBreakDown as $ownerId => $data){
    $array =[
        $ownerId,
        $data->scExpected,
        round($data->invoices_raised_total - $data->owner_payments_total, 2),
        $data->diff,
        '',
        $data->invoices_raised_total,
        $data->owner_payments_total,
    ];

    fputcsv($ownerLiabilityDiffOutStream, array_combine(HEADERS,$array));
}

fclose($ownerLiabilityDiffOutStream);

$differences = [];
foreach ($ownerBreakDown as $ownerId => $data){
    if ($data->diff > 0 || $data->diff < 0){
        $differences[$ownerId] =$data->diff;
    }
}


function getAllInvoicesRaisedForOwners($conn): array
{
    $invoicesRaisedByBooking = [];
    $sql = "SELECT b.booking_id AS owner_bookingID,
                   b._fk_property,
                   p.rcvoldID,
                   p.`_fk_owner`,
                   SUM(bf.owner_due) AS owner_due,
                   SUM(bf.commission) AS commission,
                   SUM(bf.commission_vat) AS commisison_vat
            FROM booking b
                     JOIN booking_finances bf ON b.`booking_id` = bf.`owner_bookingID`
                     JOIN property p ON p.`__pk` = b.`_fk_property`
            WHERE  bf.`invoice_date` < '2021-07-28'
            GROUP BY b.__pk
            ORDER BY b.__pk;";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            $invoicesRaisedByBooking[$obj->owner_bookingID] = $obj;
        }
    }

    return $invoicesRaisedByBooking;
}

function getAllOwnerPayments($conn)
{
    $ownerPayments = [];

    $sql = "SELECT
              b.booking_id AS owner_bookingID,
              b.`status`,
              b._fk_property,
              opo.*
            FROM
              owner_payment_owe opo
            LEFT JOIN booking b ON b.__pk = opo.`booking_id`
            WHERE opo.paid_date IS NOT NULL AND opo.paid_date <>  '0000-00-00 00:00:00' AND opo.paid_date < '2021-07-28' ";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            if ($obj->owner_bookingID == 0) {
                $obj->is_adjustment = true;
                preg_match('(\d{6,8})', $obj->payment_caption, $matches);
                if (!empty($matches)) {
                    $obj->owner_bookingID = (int)$matches[0];
                }
            } else {
                $obj->is_adjustment = false;
            }

            $ownerPayments[$obj->owner_id][] = $obj;
        }
    }
    return $ownerPayments;
}

function getAllOwners($conn)
{
    $owners = [];
    $sql = "SELECT * FROM owner";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            $owners[$obj->__pk] = (object)[
                'invoices_raised_total' => 0,
                'owner_payments_total' => 0,
                'scExpected' => 0,
                'diff' => 0,
            ];
        }
    }

    return $owners;
}

function getNewToOldOwnerMapping($conn): array
{
    $owners = [];

    $sql = "SELECT DISTINCT(`opo`.`owner_id`) AS old_owner,
               `p`.__pk           AS property_ref,
               `p`.`_fk_owner`    AS current_owner
            FROM booking b
                     JOIN property `p` ON `p`.__pk = `b`.`_fk_property`
                     JOIN owner `o` ON `o`.__pk = `p`.`_fk_owner`
                     JOIN owner_payment_owe `opo` ON `opo`.`owner_id` <> `p`.`_fk_owner` AND `opo`.`booking_id` = `b`.`__pk`
            GROUP BY CONCAT(opo.`owner_id`, '-', p.`_fk_owner`);";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            $owners[(int)$obj->current_owner][] = $obj;
        }
    }

    return $owners;
}
