<?php
include_once '../../Helpers.php';
/*
 * Outputs Update queries after parsing payment captions for booking extras having no prices
 * */

$conn = Helpers::initDB();


$query = "SELECT b._fk_property,b.booking_id,bi.* FROM booking b 
          JOIN booking_item bi ON bi._fk_booking_detail_id = b.booking_detail_id 
          WHERE bi.description NOT LIKE '%channel%' AND bi.`description` NOT LIKE '%platform%' AND bi.`description`  LIKE '%@%' AND b.`to_date` <'2021-10-01' LIMIT 1000000000";

$result       = $conn->query($query);
$bookingItems = [];

while ($obj = $result->fetchObject()) {
    $obj->parsedQuantity = 0;
    $obj->parsedPrice    = 0;
    $bookingItems[]      = $obj;
}

foreach ($bookingItems as $item) {
    $item->description = trim($item->description);

    if (stripos($item->description, 'dog') !== false || stripos($item->description, 'cat') !== false) {
        preg_match('/^\d/m', $item->description, $matches);
        $quantity = $item->quantity;
        if (! empty($matches)) {
            $item->parsedQuantity = $matches[0];
        }
    }

    preg_match('/\s?\d{2,3}/m', $item->description, $priceMatches);

    if (! empty($priceMatches)) {
        $item->parsedPrice = $priceMatches[0];
    }
}


$queries = [];

foreach ($bookingItems as $item) {
    if ($item->parsedQuantity > 0) {
        $item->price       = $item->parsedPrice / $item->parsedQuantity;
        $item->quantity    = $item->parsedQuantity;
        $item->total_price = $item->parsedPrice;
    } else {
        $item->price       = $item->parsedPrice;
        $item->total_price = $item->parsedPrice;
    }
}

foreach ($bookingItems as $item) {
    $queries[] = "UPDATE booking_item SET quantity = {$item->quantity}, price = {$item->price}, total_price = {$item->total_price} WHERE __pk = {$item->__pk};";
}

print_r($queries);
