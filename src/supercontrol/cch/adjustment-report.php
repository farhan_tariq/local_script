<?php

include_once "../../Helpers.php";
include_once "../reports/AdjustmentsReport.php";

class CCHAdjustmentsReport extends AdjustmentsReport
{
}

const EXPORT_DIRECTORY = __DIR__ . '/../../../exports';
const BRAND_ID         = '323';
const BRAND_NAME       = 'cha';
const SCHEMA           = 'character_cottages_supercontrol';
const DUMP_DATE        = '2022-01-01';

$conn   = Helpers::initDB(SCHEMA);
$report = new CCHAdjustmentsReport(
    $conn,
    EXPORT_DIRECTORY . '/'. BRAND_NAME. '/' . DUMP_DATE,
    'statement-adjustments-' . DUMP_DATE . '-Dump',
    BRAND_NAME,
    BRAND_ID
);

$report->process([]);

