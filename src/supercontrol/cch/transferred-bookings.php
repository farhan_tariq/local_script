<?php
include_once '../../Helpers.php';

const BRAND            = 'CCH';
const EXPORT_DIRECTORY = __DIR__ . '/../../exports/';
const FOLDER           = EXPORT_DIRECTORY . BRAND . '/';

$fileName = 'transferred-bookings-' . BRAND . "-" . date('Y-m-d__H-i-s') . '.csv';
$out      = fopen(FOLDER . $fileName, 'wb');

$conn        = Helpers::initDB();
$allPayments = getAllTransferredBookings($conn);
$data        = [];

$headers = [
    'bookingId',
    'status',
    'paymentcaption',
    'amount',
    'transferred_to',
    'transferred_from',
    'transferred_from_status'
];

fputcsv($out, $headers);

$count    = ['from' => 0, 'to' => 0];
$bookings = ['from' => [], 'to' => []];

foreach ($allPayments as $bookingId => $payments) {
    foreach ($payments as $payment) {
        if (! empty($payment->transferred_to)) {
            $count['to']++;
            if (! in_array($payment->booking_id, $bookings['to'])) {
                $bookings['to'][] = $payment->booking_id;
            }
        }

        if (! empty($payment->transferred_from)) {
            $count['from']++;
            if (! in_array($payment->booking_id, $bookings['from'])) {
                $bookings['from'][] = $payment->booking_id;
            }
        }

        $row    = $payment;
        $data[] = $row;
    }
}

$transferredFromBookings = [];

foreach ($data as $row) {
    if (! in_array($row->transferred_from, $transferredFromBookings) && ! empty($row->transferred_from)) {
        $transferredFromBookings[] = $row->transferred_from;
    }
}

$transferredBookingStatus = getStatusOfAllTransferredBookings($conn, $transferredFromBookings);

foreach ($data as $row) {
    $csvRow = array_combine(
        $headers,
        [
            $row->booking_id,
            $row->status,
            $row->paymentcaption,
            $row->amount,
            $row->transferred_to,
            $row->transferred_from,
            ! empty($transferredBookingStatus[$row->transferred_from]) ? $transferredBookingStatus[$row->transferred_from]->status : 'unknown'
        ]
    );

    fputcsv($out, $csvRow);
}

fclose($out);

function getAllTransferredBookings(PDO $conn)
{
    $transferred = [];

    $sql = "SELECT
             b.booking_id, b.status, bp.paymentcaption, bp.amount
            FROM
              booking_payment bp
            JOIN booking b ON b.booking_id = bp._fk_booking
            WHERE b.to_date >= '2021-10-01' ";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            $obj->transferred_from = '';
            $obj->transferred_to   = '';

            if (Helpers::array_stripos($obj->paymentcaption, ['transferred', 'to', 'from', 'trans']) !== false) {
                preg_match('(\d{6,8})', $obj->paymentcaption, $matches);
                if (! empty($matches)) {
                    if (stripos($obj->paymentcaption, 'to') !== false) {
                        $obj->transferred_to   = $matches[0];
                        $obj->transferred_from = $obj->booking_id;
                    }

                    if (stripos($obj->paymentcaption, 'from') !== false) {
                        $obj->transferred_from = $matches[0];
                        $obj->transferred_to   = $obj->booking_id;
                    }

                    $transferred[$obj->booking_id][] = $obj;
                }
            }
        }
    }

    return $transferred;
}

function getStatusOfAllTransferredBookings(PDO $conn,array $bookings)
{
    $results = [];
    $sql     = "SELECT
             b.booking_id, b.status
            FROM
              booking b
            WHERE b.booking_id IN ( " . implode(',', $bookings) . ")";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            $results[$obj->booking_id] = $obj;
        }
    }

    return $results;
}
