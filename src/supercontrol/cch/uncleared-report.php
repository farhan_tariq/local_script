<?php

include_once "../../Helpers.php";
include_once "../reports/Report.php";

class CCHUnclearedReport extends Report
{
    protected array $headers = [
        'booking_id',
        'status',
        'commission_percentage',
        'owner_property_ref',
        'property_id',
        'owner_id',
        'payment_date',
        'paid_date',
        '`payment_caption',
        'currency',
        'commission_amount',
        'commission_vat',
        'owner_due',
        'total_cash_receipts',
        'total_rent',
        '% due_to_owner',
        'channel_fee',
        'total_cash_receipts_less_channel_fee',
        'owner_liability',
        'security_deposit_reclaim',
        'booked_date',
        'from_date',
        'to_date',
    ];

    protected array $filters = [
        'AND opo.paid_date = "0000-00-00 00:00:00" '
    ];

    private array $selects = [
        'COALESCE((SELECT COALESCE(SUM(bp.amount), 0) FROM booking_payment bp  WHERE bp._fk_booking = b.booking_id GROUP BY b.__pk), 0) AS total_cash_receipts',
        '0 AS total_rent',
        "0 AS '% due_to_owner'",
        'COALESCE((SELECT COALESCE(SUM(bi.total_price), 0) FROM booking_item bi  WHERE bi._fk_booking_detail_id = b.booking_detail_id AND bi.extra_type = 95272 GROUP BY b.__pk), 0) AS channel_fee',
        '0 AS total_cash_receipts_less_channel_fee',
        '0 AS owner_liability',
        'IF(opo.payment_caption LIKE "%Security deposit fee%" , opo.amount, 0) AS security_deposit_reclaim'
    ];

    function process(array $data): array
    {
        $data       = $this->getUnclearedStatements($this->selects);
        $reportData = [];

        foreach ($data as $statementData) {
            foreach ($statementData as $row) {
                $row->owner_due                            += abs($row->security_deposit_reclaim);
                $row->total_rent                           = $row->commission_amount + $row->owner_due;
                $row->total_cash_receipts_less_channel_fee = $row->total_cash_receipts - $row->channel_fee;
                $row->{'% due_to_owner'}                   = ! empty($row->total_rent) ? ($row->owner_due / $row->total_rent) * 100 : 0;
                $row->owner_liability                      = round($row->total_cash_receipts_less_channel_fee - $row->commission_amount, 2);

                $reportData[] = $row;
            }
        }

        $this->writeToCsv($reportData);

        return $reportData;
    }
}

const EXPORT_DIRECTORY = __DIR__ . '/../../../exports';
const BRAND_ID         = '323';
const BRAND_NAME       = 'cha';
const BOOKED_DATE      = '2021-12-14';
const SCHEMA           = 'character_cottages_supercontrol';

$date = date('Y-m-d_H-i');
$conn = Helpers::initDB(SCHEMA);

$report = new CCHUnclearedReport(
    $conn,
    EXPORT_DIRECTORY . '/'. BRAND_NAME. '/' . $date,
    'uncleared-statements-' . $date,
    BRAND_NAME,
    BRAND_ID,
    [],
    BOOKED_DATE,
    BOOKED_DATE,
);

$report->process([]);
