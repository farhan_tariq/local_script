<?php

include_once "../../Helpers.php";
include_once "../reports/AdjustmentsReport.php";

class BOSAdjustmentsReport extends AdjustmentsReport
{
}

const EXPORT_DIRECTORY = __DIR__ . '/../../../exports';
const BRAND_ID         = '562';
const BRAND_NAME       = 'bos';
const SCHEMA           = 'best_of_suffolk_supercontrol';
const DUMP_DATE        = '2022-09-21';

$conn = Helpers::initDB('LOCAL', SCHEMA);

$report = new BOSAdjustmentsReport(
    $conn,
    EXPORT_DIRECTORY . '/'. BRAND_NAME. '/' . DUMP_DATE,
    'statement-adjustments-' . DUMP_DATE . '-Dump',
    BRAND_NAME,
    BRAND_ID
);

$report->process([]);

