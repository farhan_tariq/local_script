<?php

include_once "../../Helpers.php";
include_once "../reports/Report.php";

class BOSUnclearedReport extends Report
{
    protected array $headers = [
        'booking_id',
        'status',
        'commission_percentage',
        'owner_property_ref',
        'property_id',
        'owner_id',
        'payment_date',
        'paid_date',
        '`payment_caption',
        'currency',
        'commission_amount',
        'commission_vat',
        'owner_due',
        'total_cash_receipts',
        'total_rent',
        'total_extras',
        '% due_to_owner',
        'booking_fee',
        'donation',
        'booking_protect_fee',
        'total_cash_receipts_less_booking_fee',
        'owner_liability',
        'booked_date',
        'from_date',
        'to_date',
    ];

    protected array $filters = [
        'AND opo.paid_date = "0000-00-00 00:00:00" ',
        'AND opo.payment_caption NOT LIKE "%tub%" ',
        'AND opo.payment_caption NOT LIKE "%linen%" ',
        'AND opo.payment_caption NOT LIKE "%bed%" ',
        'AND opo.payment_caption NOT LIKE "%dog%" ',
        'AND opo.payment_caption NOT LIKE "%bunk%" ',
        'AND opo.payment_caption NOT LIKE "%donat%" ',
    ];

    private array $selects = [
        'COALESCE((SELECT COALESCE(SUM(bp.amount), 0) FROM booking_payment bp  WHERE bp._fk_booking = b.booking_id GROUP BY b.__pk), 0) AS total_cash_receipts',
        '0 AS total_rent',
        'COALESCE((SELECT COALESCE(SUM(bf.owner_due + bf.commission_vat + bf.commission + bf.booking_fee + bf.booking_fee_vat + bf.admin_fee + bf.other_extras_net + bf.other_extras_vat ), 0) FROM booking_finances bf  WHERE bf.owner_bookingID = b.booking_id AND bf.paymentcaption NOT LIKE "%owner%" AND bf.paymentcaption NOT LIKE "%booking fee%"  GROUP BY b.__pk), 0) AS extras',
        'COALESCE((SELECT COALESCE(SUM(bf.owner_due), 0) FROM booking_finances bf  WHERE bf.owner_bookingID = b.booking_id AND bf.paymentcaption NOT LIKE "%owner%" AND bf.paymentcaption NOT LIKE "%booking fee%" AND bf.paymentcaption NOT LIKE "%donat%" GROUP BY b.__pk), 0) AS owner_extras',
        "0 AS '% due_to_owner'",
        'COALESCE((SELECT COALESCE(SUM(bi.total_price), 0) FROM booking_item bi  WHERE bi._fk_booking_detail_id = b.booking_detail_id AND bi.extra_type = 90241 GROUP BY b.__pk), 0) AS booking_fee',
        'COALESCE((SELECT COALESCE(SUM(bi.total_price), 0) FROM booking_item bi  WHERE bi._fk_booking_detail_id = b.booking_detail_id AND bi.extra_type = 12392 GROUP BY b.__pk), 0) AS donation',
        'COALESCE((SELECT COALESCE(SUM(bi.total_price), 0) FROM booking_item bi  WHERE bi._fk_booking_detail_id = b.booking_detail_id AND (bi.description LIKE "%protect%" OR bi.description LIKE "%bp cover%" OR bi.description LIKE "%bp charge%" OR bi.description LIKE "%refunded%" OR bi.description LIKE "%bp cancel%" OR bi.description LIKE "%cancel bp%" OR bi.description LIKE "%bp refund%" OR bi.description LIKE "%bp canx%" OR bi.description LIKE "%bp cover%") GROUP BY b.__pk), 0) AS booking_protect_fee',
        '0 AS total_cash_receipts_less_booking_fee',
        '0 AS owner_liability',
    ];

    function process(array $data): array
    {
        $data       = $this->getUnclearedStatements($this->selects);
        $reportData = [];

        foreach ($data as $statementData) {
            foreach ($statementData as $row) {
                $row->total_rent                           = $row->commission_amount + $row->owner_due;
                $row->total_cash_receipts_less_booking_fee = $row->total_cash_receipts - $row->booking_fee;
                $row->owner_liability                      = round($row->total_cash_receipts_less_booking_fee - $row->commission_amount, 2);
                $row->extras                               -= $row->donation;
                $row->{'% due_to_owner'}                   = ! empty($row->total_rent) ? ($row->owner_due / $row->total_rent) * 100 : 0;
                $row->owner_due                            += $row->owner_extras;
                unset($row->owner_extras);

                $reportData[] = $row;
            }
        }

        $this->writeToCsv($reportData);

        return $reportData;
    }
}

const EXPORT_DIRECTORY = __DIR__ . '/../../../exports';
const BRAND_ID         = '562';
const BRAND_NAME       = 'bos';
const BOOKED_DATE      = '2022-09-20';
const SCHEMA           = 'best_of_suffolk_supercontrol';
const DUMP_DATE        = '2022-09-21';

$conn = Helpers::initDB('LOCAL', SCHEMA);

$report = new BOSUnclearedReport(
    $conn,
    EXPORT_DIRECTORY . '/' . BRAND_NAME . '/' . DUMP_DATE,
    'uncleared-statements-' . DUMP_DATE . '-Dump',
    BRAND_NAME,
    BRAND_ID,
    [],
    BOOKED_DATE,
    BOOKED_DATE,
);

$report->process([]);