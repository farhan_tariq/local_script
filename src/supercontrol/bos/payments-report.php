<?php
include_once "../../Helpers.php";
include_once "../reports/PaymentsReport.php";

class BOSPaymentsReport extends PaymentsReport
{

}

const EXPORT_DIRECTORY = __DIR__ . '/../../../exports';
const BRAND_ID         = '562';
const BRAND_NAME       = 'bos';
const BOOKED_DATE      = '2021-06-01';
const SCHEMA           = 'best_of_suffolk_supercontrol';
const DUMP_DATE        = '2022-09-21';
$conn = Helpers::initDB('LOCAL', SCHEMA);

$report = new BOSPaymentsReport(
    $conn,
    EXPORT_DIRECTORY . '/' . BRAND_NAME . '/' . DUMP_DATE,
    'bos-payments-report-' . DUMP_DATE . '-Dump',
    BRAND_NAME,
    BRAND_ID,
    [],
    BOOKED_DATE,
    BOOKED_DATE,
);

$report->process([]);
