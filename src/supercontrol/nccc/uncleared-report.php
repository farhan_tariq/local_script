<?php

include_once __DIR__ . "/../../Helpers.php";
include_once __DIR__ . "/../reports/Report.php";

class NorthumbriaUnclearedReport extends Report
{
    protected array $headers = [
        'booking_id',
        'status',
        'commission_percentage',
        'owner_property_ref',
        'property_id',
        'owner_id',
        'payment_date',
        'paid_date',
        'payment_caption',
        'currency',
        'commission_amount',
        'commission_vat',
        'owner_due',
        'total_cash_receipts',
        'total_rent',
        '% due_to_owner',
        'booking_fee',
        'hcp',
        'extras_to_owner',
        'extras_to_nccc',
        'total_cash_receipts_less_booking_fee_hcp',
        'owner_liability',
        'paid_to_owner_prior',
        'commission_prior',
        'extra_to_owner_2',
        'owner_due_rental_prior',
        'booked_date',
        'from_date',
        'to_date',
    ];

    protected array $filters = [
        'AND opo.paid_date = "0000-00-00 00:00:00"',
    ];

    function process(array $data): array
    {
        $data       = $this->getUnclearedStatements($this->getSelects());
        $reportData = [];

        foreach ($data as $statementData) {
            foreach ($statementData as $row) {
                $row->total_cash_receipts_less_booking_fee_hcp = $row->total_cash_receipts - $row->booking_fee - $row->hcp;
                $row->{'% due_to_owner'}                       = ! empty($row->rental_price) && ($row->rental_price > 0 || $row->rental_price < 0) ? ($row->owner_due / $row->rental_price) * 100 : 0;
                $row->owner_liability                          = round($row->total_cash_receipts_less_booking_fee_hcp - $row->commission_amount - $row->paid_to_owner_prior - $row->commission_paid_before, 2);
                $reportData[]                                  = $row;
            }
        }

        $this->writeToCsv($reportData);

        return $reportData;
    }

    function getSelects(): array
    {
        return [
            ' COALESCE((SELECT COALESCE(SUM(bp.amount), 0) FROM booking_payment bp WHERE bp._fk_booking = b.booking_id AND bp.date <= "' . $this->bookedDate . '"  GROUP BY b.__pk), 0) AS total_cash_receipts',
            ' b.rental_price',
            ' 0 AS "% due_to_owner"',
            ' COALESCE((SELECT COALESCE(SUM(bi.total_price), 0) FROM booking_item bi WHERE bi._fk_booking_detail_id = b.booking_detail_id AND (bi.extra_type = 13304 OR bi.description LIKE "%admin%" OR bi.description LIKE "%amin%" OR bi.description LIKE "%booking fee%" OR bi.description LIKE "%amend%") GROUP BY b.__pk), 0) AS booking_fee',
            ' COALESCE((SELECT COALESCE(SUM(bi.total_price), 0) FROM booking_item bi WHERE bi._fk_booking_detail_id = b.booking_detail_id AND (bi.extra_type = 13305 OR bi.description LIKE "%hcp%") GROUP BY b.__pk), 0) AS hcp',
            ' COALESCE((SELECT COALESCE(SUM(bi.total_price), 0) FROM booking_item bi WHERE bi._fk_booking_detail_id = b.booking_detail_id AND  bi.ownerprice <> 0 GROUP BY b.__pk), 0) AS extras_to_owner',
            ' COALESCE((SELECT COALESCE(SUM(bi.total_price), 0) FROM booking_item bi WHERE bi._fk_booking_detail_id = b.booking_detail_id AND bi.extra_type NOT IN (13304, 13305) AND bi.description NOT LIKE "%admin%" AND bi.description NOT LIKE "%amin%" AND  bi.description NOT LIKE "%hcp%" AND bi.ownerprice = 0 AND bi.description NOT LIKE "%booking fee%" AND bi.description NOT LIKE "%amend%" GROUP BY b.__pk), 0) AS extras_to_nccc',
            ' 0 AS total_cash_receipts_less_booking_fee_hcp',
            ' 0 AS owner_liability',
            ' COALESCE((SELECT COALESCE(SUM(opo2.amount), 0) FROM owner_payment_owe opo2  WHERE opo2.booking_id = b.__pk  AND opo2.paid_date <> "0000-00-00 00:00:00" AND opo2.paid_date <= "' . $this->bookedDate . '" GROUP BY opo2.booking_id), 0) AS paid_to_owner_prior',
            ' COALESCE((SELECT COALESCE(SUM(opo2.commission_amount), 0) FROM owner_payment_owe opo2  WHERE opo2.booking_id = b.__pk   AND opo2.paid_date <> "0000-00-00 00:00:00" AND opo2.paid_date <= "' . $this->bookedDate . '" GROUP BY opo2.booking_id), 0) AS commission_paid_before',
            ' COALESCE((SELECT COALESCE(SUM(bf.owner_due), 0) FROM booking_finances AS bf WHERE b.booking_id = bf.owner_bookingID AND bf.paymentcaption NOT LIKE "%balance%" AND bf.paymentcaption NOT LIKE "%deposit%" AND bf.paymentcaption NOT LIKE "$due to owner%"  GROUP BY bf.owner_bookingID), 0) AS extras_to_owner_2',
            ' COALESCE((SELECT COALESCE(SUM(opo2.amount), 0) FROM owner_payment_owe opo2  WHERE opo2.booking_id = b.__pk   AND opo2.paid_date <> "0000-00-00 00:00:00" AND opo2.paid_date <= "' . $this->bookedDate . '" AND (opo2.payment_caption LIKE "%balance%" OR opo2.payment_caption LIKE "%deposit%")  GROUP BY opo2.booking_id), 0) AS owner_due_rental_prior',
        ];
    }
}

$exportDirectory = __DIR__ . '/../../../exports';
$brandId         = '563';
$brandName       = 'nccc';
$schema          = 'nccc';
$dumpDate        = ! empty($argv[1]) ? $argv[1] : null;
$env             = ! empty($argv[2]) ? $argv[2] : null;

Helpers::validateScriptInput([$dumpDate, $env]);

$conn = Helpers::initDB('LOCAL', $schema);

$report = new NorthumbriaUnclearedReport(
    $conn,
    $exportDirectory . '/' . $brandName . '/' . $dumpDate,
    'uncleared-statements-' . $brandName . '-' . $dumpDate . '-Dump',
    $brandName,
    $brandId,
    [],
    $dumpDate,
    $dumpDate,
);

$report->process([]);
