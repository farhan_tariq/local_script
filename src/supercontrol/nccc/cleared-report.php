<?php

include_once __DIR__ . "/../../Helpers.php";
include_once __DIR__ . "/../reports/Report.php";

class NorthumbriaClearedReport extends Report
{
    protected array $headers = [
        'booking_id',
        'owner_property_ref',
        'property_id',
        'owner_id',
        'payment_date',
        'paid_date',
        'payment_caption',
        'currency',
        'amount',
        'commission_amount',
        'commission_vat',
        'refund',
        'supplier_id',
    ];

    protected array $filters = [
        'AND opo.paid_date <> "0000-00-00 00:00:00"',
    ];

    private array $selects = [];

    function process(array $data): array
    {
        $data       = $this->getClearedStatements($this->selects);
        $reportData = [];

        foreach ($data as $statementData) {
            foreach ($statementData as $row) {
                $reportData[] = $row;
            }
        }

        $this->writeToCsv($reportData);

        return $reportData;
    }
}

$exportDirectory = __DIR__ . '/../../../exports';
$brandId         = '563';
$brandName       = 'nccc';
$schema          = 'nccc';
$dumpDate        = ! empty($argv[1]) ? $argv[1] : null;
$env             = ! empty($argv[2]) ? $argv[2] : null;

Helpers::validateScriptInput([$dumpDate, $env]);

$conn = Helpers::initDB($env, $schema);

$report = new NorthumbriaClearedReport(
    $conn,
    $exportDirectory . '/' . $brandName . '/' . $dumpDate,
    'cleared-statements-' . $brandName . '-' . $dumpDate . '-Dump',
    $brandName,
    $brandId,
    [],
    $dumpDate,
    $dumpDate,
);

$report->process([]);
