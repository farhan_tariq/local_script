<?php
include_once "Report.php";

class PaymentsReport extends Report
{
    function process(array $data): array
    {
        $paymentsData = $this->getPaymentsData();
        $this->setHeaders(array_keys((array)$paymentsData[0]));
        $this->writeToCsv($paymentsData);

        return $paymentsData;
    }
}