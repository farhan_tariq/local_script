<?php
include "Report.php";

class AdjustmentsReport extends Report
{
    protected array $headers = [
        'booking_id',
        'status',
        'commission_percentage',
        'owner_property_ref',
        'property_id',
        'owner_id',
        'payment_date',
        'paid_date',
        '`payment_caption',
        'currency',
        'commission_amount',
        'commission_vat',
        'owner_due',
    ];

    public function process($data): array
    {
        $adjustments = !empty($data) ? $data : $this->getStatementAdjustments();
        $withBookingId = $adjustments['withId'];
        $withoutBookingId = $adjustments['withoutId'];

        $withBookingIdPropertyRef = $this->getPropertyRefUsingBooking($this->connection, array_column($withBookingId, 'booking_id'));
        $withoutBookingIdPropertyRef = $this->getPropertyRefUsingOwner($this->connection, array_column($withoutBookingId, 'owner_id'));

        foreach ($withBookingId as $adjustment) {
            if (!empty($withBookingIdPropertyRef[$adjustment->booking_id])) {
                $adjustment->status = $withBookingIdPropertyRef[$adjustment->booking_id]->status;
                $adjustment->commission_percentage = $withBookingIdPropertyRef[$adjustment->booking_id]->commission_percentage;
                $adjustment->rcvoldID = $withBookingIdPropertyRef[$adjustment->booking_id]->rcvoldID;
            }
        }

        foreach ($withoutBookingId as $adjustment){
            if (!empty($withoutBookingIdPropertyRef[$adjustment->owner_id])){
                $adjustment->rcvoldID = $withoutBookingIdPropertyRef[$adjustment->owner_id]->rcvoldID;
            }
        }

        $allAdjustments = array_merge($withBookingId, $withoutBookingId);

        $this->writeToCsv($allAdjustments);

        return $allAdjustments;
    }

    private function getPropertyRefUsingBooking(PDO $conn, array $bookings): array
    {
        $outBookings = [];

        if (!empty($bookings)) {
            $implodedBookings = implode(',', $bookings);
            $sql = "SELECT b.booking_id, b.status, b.commission_rate AS commission_percentage, p.rcvoldID 
            FROM booking b 
                LEFT JOIN property p ON p.__pk = b._fk_property
            WHERE b.booking_id IN (" . $implodedBookings . ");";

            if ($result = $conn->query($sql)) {
                while ($obj = $result->fetchObject()) {
                    $outBookings[$obj->booking_id] = $obj;
                }
            }
        }

        return $outBookings;
    }

    private function getPropertyRefUsingOwner(PDO $conn, array $owners): array
    {
        $outProperties = [];

        if (!empty($owners)) {
            $implodedOwners = implode(',', $owners);
            $sql = "SELECT p._fk_owner, p.rcvoldID 
            FROM property p 
            WHERE p._fk_owner IN (" . $implodedOwners . ");";

            if ($result = $conn->query($sql)) {
                while ($obj = $result->fetchObject()) {
                    $outProperties[$obj->_fk_owner] = $obj;
                }
            }
        }

        return $outProperties;
    }
}