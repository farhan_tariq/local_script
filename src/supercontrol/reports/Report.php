<?php

abstract class Report
{
    protected PDO $connection;
    protected string $folderPath;
    protected string $fileName;
    protected string $brandName;
    protected string $brandId;
    protected string $bookedDate;
    protected string $endDate;
    protected array $filters = [];
    protected array $headers = [];
    protected array $data;

    /**
     * @param PDO $connection
     * @param string $folderPath
     * @param string $fileName
     * @param string $brandName
     * @param string $brandId
     * @param array $data
     * @param string $bookedDate
     * @param string $endDate
     */
    public function __construct(PDO $connection, string $folderPath, string $fileName, string $brandName, string $brandId, array $data = [], string $bookedDate = '', string $endDate = '')
    {
        $this->connection = $connection;
        $this->folderPath = $folderPath;
        $this->fileName   = $fileName;
        $this->brandName  = $brandName;
        $this->brandId    = $brandId;
        $this->data       = $data;
        $this->bookedDate = $bookedDate;
        $this->endDate    = $endDate;
    }

    abstract function process(array $data): array;

    public function getUnclearedStatements(array $selects): array
    {
        $select = implode(',' . PHP_EOL . "\t\t", $selects) . ',';

        $sql = 'SELECT
              b.booking_id,
              b.status,
              p.commission_percentage,   
              p.rcvoldID,  
              p.__pk,
              opo.`owner_id`,
              opo.`payment_date`,
              opo.`paid_date`,
              GROUP_CONCAT(opo.`payment_caption` SEPARATOR ", ") AS payment_caption,
              opo.`currency`,
              SUM(opo.`commission_amount`) AS commission_amount,
              opo.`commission_vat`,
              SUM(opo.`amount`) AS owner_due, 
              ' . $select . '
              b.booked_date,
              b.from_date,
              b.to_date
              FROM
              owner_payment_owe opo
              JOIN booking b ON b.__pk = opo.`booking_id`
              JOIN property p ON p.__pk = b.`_fk_property`
              JOIN owner o ON o.__pk = p._fk_owner  
            WHERE b.booked_date <= "' . $this->bookedDate . '" ';

        $sql .= implode(' ', $this->filters);
        $sql .= '  GROUP BY b.booking_id LIMIT 999999999999';

        if ($result = $this->connection->query($sql)) {
            while ($obj = $result->fetchObject()) {
                $this->data[$obj->booking_id][] = $obj;
            }
        }

        return $this->data;
    }

    public function getClearedStatements(array $selects): array
    {
        $select = implode(',' . PHP_EOL . "\t\t", $selects);

        $sql = "SELECT
              b.booking_id,
              p.`rcvoldID` AS owner_property_ref,
              p.__pk AS property_id,
              p.`_fk_owner` AS owner_id,
              opo.`payment_date`,
              opo.`paid_date`,
              REPLACE(REPLACE(opo.`payment_caption`, ',', ' -'),' 	','') AS payment_caption,
              opo.`currency`,
              opo.`amount`, 
              opo.`commission_amount`,
              opo.`commission_vat`,
              opo.`refund`,
              opo.`supplier_id`
              {$select}  
              FROM
              owner_payment_owe opo
              JOIN booking b ON b.__pk = opo.`booking_id`
              JOIN property p ON p.__pk = b.`_fk_property`
              JOIN owner o ON o.__pk = p._fk_owner  
            WHERE b.booked_date <= '{$this->bookedDate}' " ;

        $sql .= implode(' ', $this->filters);
        $sql .= ' GROUP BY opo.`__pk` LIMIT 999999999999';

        if ($result = $this->connection->query($sql)) {
            while ($obj = $result->fetchObject()) {
                $this->data[$obj->booking_id][] = $obj;
            }
        }

        return $this->data;
    }

    public function getStatementAdjustments(): array
    {
        $adjustmentsWithBookingId    = [];
        $adjustmentsWithOutBookingId = [];

        $sql = " SELECT
              p.__pk,  
              opo.`owner_id`,
              opo.`payment_date`,
              opo.`paid_date`,
              opo.`payment_caption`,
              opo.`currency`,
              opo.`commission_amount`,
              opo.`commission_vat`,
              opo.`amount`
            FROM
              owner_payment_owe opo
              JOIN property p
                ON p.`_fk_owner` = opo.`owner_id`
            WHERE  opo.`booking_id` = 0 ";

        $sql .= implode(' ', $this->filters);

        if ($result = $this->connection->query($sql)) {
            while ($obj = $result->fetchObject()) {
                preg_match('(\d{6,8})', $obj->payment_caption, $matches);

                if (! empty($matches)) {
                    $merged_temp_object         = (object) array_merge(['booking_id' => $matches[0], 'status' => 'unknown', 'commission_percentage' => 0, 'rcvoldID' => ''], (array) $obj);
                    $adjustmentsWithBookingId[] = $merged_temp_object;
                } else {
                    $merged_temp_object            = (object) array_merge(['booking_id' => 0, 'status' => 'unknown', 'commission_percentage' => 0, 'rcvoldID' => ''], (array) $obj);
                    $adjustmentsWithOutBookingId[] = $merged_temp_object;
                }
            }
        }

        return [
            'withId'    => $adjustmentsWithBookingId,
            'withoutId' => $adjustmentsWithOutBookingId
        ];
    }

    public function getPaymentsData(): array
    {
        $sql = 'SELECT
                  b.booking_id,
                  b.`_fk_property`,
                  REPLACE(p.`name`,"-",",") AS property_name,
                  b.`_fk_customer`,
                  c.`first_name` AS customer_first_name,
                  c.`last_name` AS customer_last_name,
                  b.`status`,
                  b.`from_date`,
                  b.`to_date`,
                  b.`booking_type`,
                  bp.`date` AS payment_date,
                  bp.`paymentcaption`,
                  bp.`payment_method`,
                  bp.`amount` AS payment_amount
                FROM
                  booking b
                  JOIN booking_payment bp ON b.`booking_id` = bp.`_fk_booking`
                    JOIN property p ON p.__pk = b.`_fk_property`
                    JOIN customer c ON c.`__pk` = b.`_fk_customer`
                WHERE b.`from_date` >= "' . $this->endDate . '" ORDER BY bp.date;';

        if ($result = $this->connection->query($sql)) {
            while ($obj = $result->fetchObject()) {
                $this->data[] = $obj;
            }
        }

        return $this->data;
    }

    public function writeToCsv(array $data): void
    {
        if (! file_exists($this->folderPath)) {
            mkdir($this->folderPath, 0777, true);
        }

        $writer = fopen($this->folderPath . '/' . $this->fileName . '.csv', 'wb');

        fputcsv($writer, $this->headers);

        foreach ($data as $row) {
            $row = array_combine($this->headers, (array) $row);
            fputcsv($writer, $row);
        }

        fclose($writer);

        echo 'Data written to CSV file: ' . $this->folderPath . '/' . $this->fileName . '.csv' . PHP_EOL;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @param array $filters
     */
    public function setFilters(array $filters): void
    {
        $this->filters = $filters;
    }
}
