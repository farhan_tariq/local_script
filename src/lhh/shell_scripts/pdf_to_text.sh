#!/usr/bin/env bash
fullPath=$1

cashInvoicesPdfFolder="$fullPath/Cash-Invoices-PDF/"
commissionInvoicesPdfFolder="$fullPath/Commission-Invoices-PDF/"

cashInvoicesTextFolder="$fullPath/Cash-Invoices-Text/"
commissionInvoicesTextFolder="$fullPath/Commission-Invoices-Text/"

echo 'All files to be moved'
#find "$fullPath" -type f -iregex '.*[ \n\t]cm.pdf'

echo 'Converting Cash Invoices PDF to Text'
find "$cashInvoicesPdfFolder" -maxdepth 1 -type f | while read  x; do pdftotext -layout "$x"; done

echo 'Creating Cash Invoices Text Directory'
mkdir -p "$cashInvoicesTextFolder"

echo 'Moving Converted Cash Invoices to New Folder'
find "$cashInvoicesPdfFolder" -maxdepth 1 -type f -iregex '.*[ \n\t]cm.txt' | while read  x; do mv "$x" "$cashInvoicesTextFolder";done

echo 'Converting Commission Invoices PDF to Text'
find "$commissionInvoicesPdfFolder" -maxdepth 1 -type f | while read  x; do pdftotext -layout "$x"; done

echo 'Creating Commission Invoices Text Directory'
mkdir -p "$commissionInvoicesTextFolder"

echo 'Moving Converted Cash Invoices to New Folder'
find "$commissionInvoicesPdfFolder" -maxdepth 1 -type f -iregex '.*.txt' | while read  x; do mv "$x" "$commissionInvoicesTextFolder";done
