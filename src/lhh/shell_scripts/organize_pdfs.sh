#!/usr/bin/env bash
fullPath=$1

allInvoicesPDFFolder="$fullPath/All-Invoices/"
cashInvoicesPDFFolder="$fullPath/Cash-Invoices-PDF/"
commissionInvoicesPDFFolder="$fullPath/Commission-Invoices-PDF/"

echo 'All files to be moved'
#find "$fullPath" -type f -iregex '.*[ \n\t]cm.pdf'

echo 'Creating Cash Invoices Directory'
mkdir -p "$cashInvoicesPDFFolder"

echo 'Moving files to Cash Invoices Directory'
find "$fullPath" -maxdepth 1 -type f -iregex '.*[ \n\t]cm.pdf' | while read  x; do mv "$x" "$cashInvoicesPDFFolder"; done

echo 'Creating Commission Invoices Directory'
mkdir -p "$commissionInvoicesPDFFolder"

echo 'Moving files to Commission Invoices Directory'
find "$fullPath" -maxdepth 1 -type f  | while read  x; do mv "$x" "$commissionInvoicesPDFFolder"; done

echo 'Creating All Invoices Directory'
mkdir -p "$allInvoicesPDFFolder"

echo 'Copying files to All Invoices Directory'
find "$fullPath" -type f | while read  x; do cp "$x" "$allInvoicesPDFFolder"; done
