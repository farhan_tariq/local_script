<?php
include "Statement.php";

class CommissionInvoiceStatement extends Statement
{
    public function process(): array
    {
        $this->processPayments();

        foreach ($this->payments as $index => $payment) {
            [$customerName, $bookingId, $startDate, $endDate] = '';

            if (str_contains($payment, 'above')) {
                $customerName = $this->rows[$index - 1]->customer_name;
                $bookingId    = $this->rows[$index - 1]->booking_id;
                $startDate    = $this->rows[$index - 1]->start_date;
                $endDate      = $this->rows[$index - 1]->end_date;
            }

            $row = $this->getStructure();
            $row->booking_id    = !empty($bookingId) ? $bookingId : $this->getId($payment);
            $row->customer_name = !empty($customerName) ? $customerName : $this->getCustomerName($payment);
            $row->start_date    = !empty($startDate) ? $startDate : $this->getBookingStartDate($payment);
            $row->end_date      = !empty($endDate) ? $endDate : $this->getBookingEndDate($payment);
            $row->description   = $payment;
            $row->amount        = $this->getPaymentAmount($payment);

            $this->rows[] = $row;
        }

        $finalRow = $this->getStructure();
        $finalRow->vat_total = $this->vatTotal;
        $finalRow->total = $this->total;
        $this->rows[] = $finalRow;

        return $this->rows;
    }

    private function processPayments()
    {
        $keys = [];
        foreach ($this->payments as $index => $payment) {
            if (!str_contains($payment, 'S2')) {
                $this->payments [$index - 1] .= ' ' . $payment;
                $keys[] = $index;
            }
        }

        foreach ($keys as $key) {
            unset($this->payments[$key]);
        }
    }

    public function getStructure(): object
    {
        return (object )[
            'owner_title' => $this->ownerTitle,
            'owner_name' => $this->ownerName,
            'invoice_no' => $this->invoiceNo,
            'tax_date' => $this->taxDate,
            'propertyName' => $this->propertyName,
            'booking_id' => '',
            'customer_name' => '',
            'start_date' => '',
            'end_date' => '',
            'description' => '',
            'rental_amount' => '',
            'commission_percentage' => '',
            'amount' => '',
            'vat_total' => '',
            'total' => ''
        ];
    }
}

$statement = new CommissionInvoiceStatement($argv[1]);
$statement->readFile();
$statement->processItems();
$statement->process();
$statement->printStatement();