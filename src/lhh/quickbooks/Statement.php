<?php
include "../../Helpers.php";

abstract class Statement
{
    protected string $fileName;
    protected string $ownerTitle = '';
    protected string $ownerName = '';
    protected string $propertyName = '';
    protected string $invoiceNo = '';
    protected string $taxDate = '';
    protected string $vatTotal = '';
    protected string $total = '';
    protected array $ignore;
    protected array $items;
    protected array $payments;
    protected array $rows;

    /**
     * @param string $fileName
     * @param array $ignore
     */
    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
        $this->ignore = $this->getIgnoredLines();
        $this->items = [];
        $this->rows = [];
        $this->payments = [];
    }

    abstract function process(): array;

    abstract function getStructure(): object;

    public function readFile(): bool
    {
        $read = fopen($this->fileName, 'r');
        if (!$read) {
            echo 'File not found: ' . $this->fileName;
            return false;
        }

        while (!feof($read)) {
            $str = trim(fgets($read));
            $str = str_replace("\n", '', $str);
            $str = str_replace("\f", '', $str);
            $str = preg_replace('/\s\s+/', ' ', $str);

            if (empty($str) || Helpers::array_stripos($str, $this->ignore)) {
                continue;
            }

            $this->items[] = $str;
        }
        fclose($read);

        $this->propertyName = $this->getPropertyName($this->fileName);

        return true;
    }

    public function processItems()
    {
        $start = 0;
        $end = 0;

        foreach ($this->items as $index => $item) {
            $string = str_replace(PHP_EOL, '', trim(preg_replace('/\s+/', ' ', $item)));

            if ($end > 0 && $index > $end) {
                continue;
            }

            if (!empty($string) && !$this->ownerName) {
                $this->ownerName = $this->getOwnerName($item);
                $this->ownerTitle = $this->getOwnerTitle($item);
                continue;
            }

            if (str_contains($item, 'Invoice No')) {
                $this->invoiceNo = $this->getId($item);
                continue;
            }

            if (str_contains($item, 'Tax Date')) {
                $this->taxDate = $this->getTaxDate($item);
                continue;
            }

            if ($string === 'Booking VAT Amount' || $string === 'Booking Amount') {
                $start = $index + 1;
                continue;
            }

            if ($start > 0 && $index >= $start && !empty($string) && !str_contains($string, 'VAT Total')) {
                $this->payments[] = $item;
                continue;
            }

            if (str_contains($item, 'VAT Total')) {
                $end = $index;
                $this->total = $this->getTotal($item);
                $this->vatTotal = $this->getVatTotal($item);
            }
        }
    }

    public function getOwnerTitle($string): string
    {
        $re = '/(?i)(Mr.&.Mrs)|(?i)((M(?:is)?s|(Mrs?))|Dr|DR)/m';
        preg_match($re, $string, $matches);

        if (!empty($matches)) {
            return $matches[0];
        }

        return '';
    }

    public function getOwnerName($string): string
    {
        $re = '/(?i)(Mr.&.Mrs)?.*|(?i)((M(?:is)?s|(Mrs?))|Dr|DR)?.*/m';
        preg_match($re, $string, $matches);

        if (!empty($matches) && count($matches) > 1) {
            return trim(str_replace($matches[1], '', $matches[0]));
        }

        return trim($matches[0]);
    }

    public function getPropertyName($string): string
    {
        $string = preg_split('/[A-Za-z]{3}\s\d{1,2}/m', $string);
        $split = explode('/', $string[0]);

        return trim($split[count($split) - 1]);
    }

    function getId($string): string
    {
        $re = '/\d{5}/m';
        preg_match($re, $string, $matches);

        if (!empty($matches)) {
            return $matches[0];
        }

        return '';
    }

    function getTaxDate($string): string
    {
        $re = '/[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}/m';
        preg_match($re, $string, $matches);

        if (!empty($matches)) {
            return $matches[0];
        }

        return '';
    }

    public function getVatTotal($string): string
    {
        $split = explode('TOTAL', $string);
        $re = '/-?\d+\.\d{2}/m';
        preg_match($re, $split[0], $matches);

        return !empty($matches) ? $matches[0] : '';
    }

    public function getTotal($string): string
    {
        $string = str_replace([',', '£'], '', $string);
        $split = explode('TOTAL', $string);
        $re = '/-?\d+\.\d{2}/m';
        preg_match($re, $split[1], $matches);

        return !empty($matches) ? $matches[0] : '';
    }

    public function getPaymentAmount($string): string
    {
        $string = str_replace(',', '', $string);
        $re = '/-?\d+\.\d{2}/m';
        preg_match($re, $string, $matches);

        return !empty($matches) ? $matches[0] : '';
    }

    public function getBookingStartDate($string): string
    {
        $startDate = '';
        $re = '/(\d?\d\/\d?\d|\d?\d)-(\d?\d\/\d?\d)\/(\d{2,4})/m';
        preg_match($re, $string, $matches);

        if (!empty($matches)) {
            $year = strlen($matches[3]) === 2 ? '20' . $matches[3] : $matches[3];
            if (strlen($matches[1]) > 2) {
                $startDate .= $matches[1] . '/' . $year;
            } else {
                $split = explode('/', $matches[2]);
                $startDate = $matches[1] . '/' . $split[1] . '/' . $year;
            }
        }

        return $startDate;
    }

    public function getBookingEndDate($string): string
    {
        $re = '/(\d?\d\/\d?\d|\d?\d)-(\d?\d\/\d?\d)\/(\d{2,4})/m';
        preg_match($re, $string, $matches);

        if (!empty($matches)) {
            $year = strlen($matches[3]) === 2 ? '20' . $matches[3] : $matches[3];
            return $matches[2] . '/' . $year;
        }

        return '';
    }

    public function getCustomerName($string): string
    {
        $re = '/([A-Za-z]+-?[A-Za-z]+?)\s?(\(\d{5}\))?\s+?(\d?\d|\d?\d\/\d?\d)-(\d?\d\/\d?\d\/\d{2,4})/m';
        preg_match($re, $string, $matches);

        if (!empty($matches)) {
            return $matches[1];
        }

        return '';
    }

    public function getIgnoredLines(): array
    {
        return [
            'LHH Ltd',
            'LHH Scotland',
            'LHHScotland',
            'IV7 8LQ',
            '761615923',
            'Payments taken from ',
        ];
    }

    public function printStatement()
    {
        foreach ($this->rows as $row) {
            $outputString = '';
            foreach ($row as $cell) {
                $outputString .= $cell . ',';
            }

            echo substr($outputString, 0, -1) . PHP_EOL;
        }
    }
}