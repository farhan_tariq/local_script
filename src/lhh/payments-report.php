<?php
include '../supercontrol/reports/PaymentsReport.php';
include '../Helpers.php';

class LHHPaymentsReport extends PaymentsReport
{
    public function getPaymentsData(): array
    {
        $sql = "SELECT bd.id                       AS id,
                       bd.`customer`               AS _fk_customer,
                       FROM_UNIXTIME(bd.`created`) AS booked_date,
                       bd.`start_date`             AS from_date,
                       bd.`end_date`               AS to_date,
                       CASE bd.status
                           WHEN - 1 THEN 'Cancelled'
                           WHEN 0 THEN 'Unsubmitted'
                           WHEN 1 THEN 'In Progress'
                           WHEN 3 THEN 'Confirmed'
                           WHEN 4 THEN 'Paid In Full'
                           WHEN 5 THEN 'Confirmed'
                           ELSE 'Owner booking'
                           END                     AS status,
                       bd.canceled,
                       bd.booking_created_by,
                       p.`amount__number`          AS amount,
                       p.`amount__currency_code`,
                       CASE p.payment_status
                           WHEN 0 THEN 'DEFERRED'
                           WHEN 1 THEN 'RELEASED'
                           WHEN 2 THEN 'VOID'
                           WHEN 3 THEN 'REFUNDED'
                           WHEN 4 THEN 'ABORT_BACS'
                           WHEN 5 THEN 'BANKED'
                           WHEN 6 THEN 'DESTROYED'
                           WHEN 8 THEN 'AUTHORSISE'
                           WHEN 9 THEN 'AUTHENTICATE'
                           WHEN 10 THEN 'CANCEL'
                           WHEN 11 THEN 'ABORT'
                           WHEN 12 THEN 'PAYMENT'
                           ELSE 'UNKNOWN' END      AS payment_status,
                       CASE p.payment_type
                           WHEN 0 THEN 'BALANCE_PAYMENT'
                           WHEN 1 THEN 'FULL_PAYMENT'
                           WHEN 2 THEN 'GOOD_HOUSE_KEEPING_DEPOSIT'
                           WHEN 3 THEN 'ADDITIONAL_DEPOSIT_PAYMENT'
                           WHEN 4 THEN 'OUTSTANDING_DEPOSIT'
                           WHEN 5 THEN 'DEPOSIT_PAYMENT'
                           WHEN 6 THEN 'REFUND_PAYMENT'
                           WHEN 7 THEN 'PART_BALANCE_PAYMENT'
                           ELSE 'UNKNOWN' END      AS payment_type,
                           DATE(`p`.`payment_date`) as payment_date
                FROM lhh_booking AS b
                         JOIN lhh_booking_data bd ON bd.vid = b.vid
                         JOIN lhh_payment p ON p.booking = b.id
                WHERE (
                (DATE(bd.`booking_date`) >= '2019-01-01' AND DATE(bd.booking_date) <= '2022-06-29') OR 
                (DATE(bd.booking_date) <= '2022-06-29' AND YEAR(bd.start_date) > 2021)
                       )
                    AND DATE(p.`payment_date`) <= '2022-06-29'";

        if ($result = $this->connection->query($sql)) {
            while ($obj = $result->fetchObject()) {
                $this->data[] = $obj;
            }
        }

        return $this->data;
    }
}

const EXPORT_DIRECTORY = __DIR__ . '/../../exports';
const BRAND_ID         = '564';
const BRAND_NAME       = 'lhh';
const BOOKED_DATE      = '2019-01-01';
const SCHEMA           = 'lhh';
const DUMP_DATE        = '2022-06-28';

$conn = Helpers::initDB('LOCAL',SCHEMA);

$report = new LHHPaymentsReport(
    $conn,
    EXPORT_DIRECTORY . '/'. BRAND_NAME. '/' . DUMP_DATE,
    'lhh-payments-report-' . DUMP_DATE . '-Dump',
    BRAND_NAME,
    BRAND_ID,
    [],
    BOOKED_DATE
);

$report->process([]);